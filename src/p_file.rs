use std::fmt;

use crate::parseparts::G;

#[derive(Debug, Clone)]
pub struct PFile {
    pub pfile: Vec<G>,
}

impl fmt::Display for PFile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        trace!("Pringint the parsed file: {:?}", self);
        for l in self.pfile.clone() {
            trace!("Pringint the parsed line: {:?}", l);
            write!(f, "{}", l)?;
        }
        Ok(())
    }
}

impl PFile {
    pub fn new(pfile: Vec<G>) -> Self {
        Self { pfile }
    }
    //pub fn vec(&self) -> Vec<G> {
    //    self.pfile
    //}
}
