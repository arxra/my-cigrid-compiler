#![allow(clippy::clippy::clippy::useless_format)]

use std::fmt;

use crate::{
    errors::AppError,
    p_file::PFile,
    parseparts::{e::E, s::S, G},
    tokens::ty::Ty,
};

/// This function is here to ease possible refactoring down the road. Instead of working with the
/// native string type we instead use this custom type. This is a recommendation from David, as he
/// is using ints here instead which can go find the string in a hashmap. That's more advanced than
/// what I want, but I'll still take the sym wrapper.
#[derive(Debug, Clone, PartialEq)]
pub struct Sym {
    pub sym: String,
}

impl Sym {
    pub fn new(sym: String) -> Self {
        Self { sym }
    }
}

impl fmt::Display for Sym {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.sym)
    }
}

/// For S level, these are not used but they are present for extensibilitys sake to highers grades.
#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum Jbinop {
    Jl,
    Jg,
    Jle,
    Jge,
    Je,
    Jne,
}

/// The end of a block is signalled by one of several endings.
/// For S level, only Ret is used.
/// For higher level, we might need to jump back to main or other nested functions.
#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum Blockend {
    IRSReturn(Option<E>),
    IRSBranch(Jbinop, Sym, Sym),
    IRSJump(Sym),
}

impl fmt::Display for Blockend {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Blockend::IRSReturn(e) => {
                if let Some(e) = e {
                    writeln!(f, "IRSReturn({})", e)
                } else {
                    writeln!(f, "IRSReturn()")
                }
            }
            e => todo!(
                "other blockends than return are for higher than s level. got a {}",
                e
            ),
        }
    }
}

/// Each statement is only one of 3 kinds.
/// Note that this should be enough for S and G levels, for VG this will need to be expanded to
/// handle more cases. Note the types here however, I am not 100% sure they will be enough.
#[derive(Debug, Clone)]
pub enum IRStmt {
    IRSExpr(E),
    IRSVarAssign(Sym, E),
    IRSVarDecl(Sym, Ty),
}

impl fmt::Display for IRStmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            IRStmt::IRSExpr(e) => {
                write!(f, "IRSExpr{}", e)
            }
            IRStmt::IRSVarAssign(sym, e) => {
                write!(f, "IRSVarAssign({}, {})", sym, e)
            }
            IRStmt::IRSVarDecl(sym, ty) => {
                write!(f, "IRSVarDecl({}, {})", sym, ty)
            }
        }
    }
}

/// The basic block for constructing assembler. These make sense when you look at labels in
/// assembler, where these will be used. For S level, the entire function will be a single block
#[derive(Debug, Clone)]
pub struct IRBlock {
    pub sym: Sym, // This should be unique. Keep a list of them and ensure its unique
    pub stms: Vec<IRStmt>,
    pub end: Blockend, // is it a jump or a return?
}

impl IRBlock {
    /// Use a symbol to name the block, and then take some Statement and parse the
    /// statement into IR parts, results in a complete IRBlock.
    fn new(sym: Sym, s: S) -> Result<Self, AppError> {
        let end = Blockend::IRSReturn(None);

        let mut stms = Vec::new();
        if let S::SScope(scope) = s {
            for s in scope {
                match *s {
                    S::SVarDef(ty, name, e_val) => {
                        let sy = Sym::new(name);
                        stms.push(IRStmt::IRSVarDecl(sy.clone(), ty));
                        stms.push(IRStmt::IRSVarAssign(sy, e_val));
                    }
                    S::SExpr(e) => stms.push(IRStmt::IRSExpr(e)),
                    S::SVarAssign(sym, e) => stms.push(IRStmt::IRSVarAssign(Sym::new(sym), e)),
                    S::SReturn(ret) => {
                        return Ok(Self {
                            sym,
                            stms,
                            end: Blockend::IRSReturn(ret),
                        })
                    }
                    //S::SScope(scope) => {}
                    //S::SIf(_, _, _) => {}
                    //S::SArrayAssign(_, _, _, _) => {}
                    //S::SWhile(_, _) => {}
                    //S::SBreak => {}
                    //S::SDelete(_) => {}
                    a => todo!("cannot parse {} to IRStmt", a),
                }
            }
        } else {
            return Err(AppError::new(1, format!("could not parse as block: {}", s)));
        }
        Ok(Self { sym, stms, end })
    }
}

impl fmt::Display for IRBlock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "\nIRBlock({{{},", self.sym)?;
        for s in self.stms.clone() {
            writeln!(f, "{}", s)?;
        }
        writeln!(f, "{}", self.end)?;
        writeln!(f, "}})")
    }
}

/// Paramater pairings of type and a symbol(representative name).
#[derive(Debug, Clone)]
pub struct Param {
    ty: Ty,
    sym: Sym,
}

impl fmt::Display for Param {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.ty, self.sym)
    }
}

/// The first string in the globals is the name of the function. The other half of the touple
/// is the other parts of that function, starting with the return type of the function, the
/// input paraemters, and lastly the basic blocks which makes up the function.
/// The basic blocks are usefull to convert the AST to ASM blocks, where each basic block can
/// have its own label. Labels are needed for greater than S level, but also very usefull for
/// debugging in GDB.
///
#[allow(clippy::clippy::type_complexity)]
#[derive(Debug, Clone)]
pub struct IRFunc {
    pub ty: Ty,
    pub sym: Sym,
    pub params: Vec<Param>,
    pub blocks: Vec<IRBlock>,
}

impl IRFunc {
    pub fn new(global: G) -> Result<Self, AppError> {
        match global {
            G::GFuncDef(gfd) => {
                let sym = Sym::new(gfd.ident);
                let params = Vec::new();
                let mut blocks: Vec<IRBlock> = Vec::new();
                blocks.push(IRBlock::new(sym.clone(), gfd.scope)?);
                Ok(Self {
                    ty: gfd.ty,
                    sym,
                    params,
                    blocks,
                })
            }
            G::GFuncDecl(ty, sym, pars) => {
                // This is handling external functions.
                let mut params = Vec::new();
                for (t, sy) in pars.pars {
                    params.push(Param {
                        ty: t,
                        sym: Sym::new(sy),
                    })
                }
                Ok(Self {
                    ty,
                    sym: Sym::new(sym),
                    params,
                    blocks: Vec::new(),
                })
            }
            a => todo!("handle {}", a),
        }
    }
}
impl fmt::Display for IRFunc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "IRFunc({}, {}, {{", self.ty, self.sym)?;
        for i in self.params.clone() {
            write!(f, "{}", i)?;
        }

        write!(f, "}},")?;
        for b in self.blocks.clone() {
            write!(f, "\n{}", b)?;
        }
        write!(f, ")")
    }
}

#[derive(Debug, Clone)]
pub struct IRProg {
    pub funcs: Vec<IRFunc>,
}

impl IRProg {
    pub fn new(pfile: PFile) -> Result<Self, AppError> {
        let mut funcs = Vec::new();
        for g in pfile.pfile {
            funcs.push(IRFunc::new(g)?);
        }
        Ok(IRProg { funcs })
    }
}
impl fmt::Display for IRProg {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for fun in self.funcs.clone() {
            write!(f, "IRFunc({}, {}, ", fun.ty, fun.sym)?;
            // print the params
            write!(f, "{{")?;
            for par in fun.params {
                write!(f, "{}", par)?;
            }
            write!(f, "}}, ")?;
            //print the blocks
            for block in fun.blocks {
                write!(f, "{}", block)?;
            }
            write!(f, ")")?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {

    use crate::{parser::parse, scanner::scan};

    use super::*;

    #[test]
    fn trivial() -> Result<(), AppError> {
        let mut scanned = scan("examples/ir_triv.cpp")?;
        let parsed = parse(&mut scanned)?;
        let _ = IRProg::new(parsed)?;
        Ok(())
    }
    #[test]
    fn simple() -> Result<(), AppError> {
        let mut scanned = scan("examples/ir_simp.cpp")?;
        let parsed = parse(&mut scanned)?;
        let _ = IRProg::new(parsed)?;

        Ok(())
        //Err(AppError::new(1, "".to_string()))
    }
}
