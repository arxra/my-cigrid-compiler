use crate::errors::AppError;
use crate::p_file::PFile;
use crate::parseparts::e::E;
use crate::parseparts::s::S;
use crate::parseparts::GFuncDef;
use crate::parseparts::GVarDec;
use crate::parseparts::GVarDef;
use crate::parseparts::Params;
use crate::parseparts::G;
use crate::scanner::file::File;
use crate::tokens::binop::Binop;
use crate::tokens::delims::Delim;
use crate::tokens::expr::Expr;
use crate::tokens::keywords::Kword;
use crate::tokens::ty::Ty;
use crate::tokens::Tok;

// Return Prog(g)
pub fn parse(file: &mut File) -> Result<PFile, AppError> {
    info!("Parsing data: {:#?}", file);
    if file.is_empty() {
        warn!("Got a empty file, returning immideatly with empty");
        return Err(AppError::new(0, format!("")));
    }
    let parsed = parse_file(file)?;
    debug!("Parsed data: {:#?}", parsed);
    let pfile = PFile::new(parsed);

    info!("Remaining file: {:#?}", file);

    Ok(pfile)
}

fn parse_file(file: &mut File) -> Result<Vec<G>, AppError> {
    // The entire progam 'p' is just v(g), where
    // v: vector
    // g: various declarations
    let mut res: Vec<G> = Vec::new(); // literally v(g)
    while let Some(token) = file.peek() {
        trace!(
            "There are tokens left (next: {:?}), so parsing for 'g'",
            token
        );
        let g = parse_g(file)?;
        debug!("Parsed g:{:#?}", g);
        res.push(g);
    }
    Ok(res)
}

/// These are the valid G matches of the abstract syntax.
/// Addition: v denotes list of () expressions.
/// T: Type
/// r: Range over text string (identifier)
/// s: Scope
/// e: Expr
///
/// Valid matches:
/// GFuncDef         T,r,v(T,r),s
/// GFuncDecl        T,r,v(T,r)
/// GVarDef          T,r,e
/// GVarDecl         T,r
/// GStruct          r,v(T,r)
fn parse_g(file: &mut File) -> Result<G, AppError> {
    let next = file.next();
    if let Some(next) = next {
        match next.tok() {
            // Should find a T or a r, and if T then immideatly r
            Tok::Ty(ty) => {
                // We have T
                // We must then also have 'r'
                trace!(
                    "Found a type: {:?}\t\tnow ensuring there is a identifier",
                    ty
                );
                if let Tok::Expr(Expr::Ident(ident)) = file.nerr()?.tok() {
                    // We have T and r. Peek next to see how to handle it.
                    //
                    debug!("Parsed {} {}, is it a function?", ty, ident);
                    if let Some(peeked) = file.peek() {
                        match peeked.tok() {
                            Tok::Delim(Delim::LPar) => {
                                let func = parse_func(file, ty, ident)?;
                                info!("Finished parsing partial global: {:?}", func);
                                return Ok(G::GFuncDef(func));
                            }
                            Tok::Delim(Delim::Eq) => {
                                file.next();
                                let val = parse_expr(file, 0)?;
                                if let Some(val) = val {
                                    let glob = G::GVarDef(GVarDef::new(ty, ident, val));
                                    info!("Finished parsing partial global: {:?}", glob);
                                    return Ok(glob);
                                } else {
                                    let err = AppError::new(
                                        1,
                                        format!("Found = and ; but no body in between"),
                                    );
                                    return Err(err);
                                }
                            }
                            Tok::Delim(Delim::SemiCol) => {
                                return Ok(G::GVarDec(GVarDec::new(ty, ident)))
                            }

                            _ => todo!("invalid next peek in parse g: {:?}", peeked),
                        }
                    } else {
                        return Err(AppError::new(
                            1,
                            format!("parse_g: no more tokens after ident"),
                        ));
                    }
                } else {
                    let err = AppError::new(1, format!("Found type but no identifier"));
                    return Err(err);
                }
            }
            Tok::Kword(Kword::Extern) => {
                if let Tok::Ty(ty) = file.nerr()?.tok() {
                    if let Tok::Expr(Expr::Ident(ident)) = file.nerr()?.tok() {
                        match file.nperr()?.tok() {
                            Tok::Delim(Delim::SemiCol) => {
                                return Ok(G::GVarDec(GVarDec::new(ty, ident)))
                            }
                            Tok::Delim(Delim::LPar) => {
                                file.next(); // The peek is done, no need for the val
                                trace!("parse_g: looks like extern function def");
                                let params = parse_func_params(file)?;
                                info!("parse_g: Parsed parameters: {:?}", params);
                                file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                                return Ok(G::GFuncDecl(ty, ident, params));
                            }
                            e => {
                                return Err(AppError::new(
                                    1,
                                    format!("Uknonw foffolwer of extern type: {:?}", e),
                                ))
                            }
                        }
                    }
                }
            }
            Tok::Kword(Kword::Struct) => {
                // GStruct(r, v(ty, r))
                let mut ident = String::new();
                if let Some(next) = file.next() {
                    if let Tok::Expr(Expr::Ident(i)) = next.tok() {
                        ident = i;
                    }
                }
                file.next(); // Assuming curly bois
                let mut vars: Vec<(Ty, String)> = Vec::new();
                'q: while let Some(next) = file.next() {
                    trace!("parse_g: structs: Now parsing {:?}", next);
                    if let Tok::Delim(Delim::RCurl) = next.tok() {
                        file.next();
                        break 'q;
                    }
                    if let Tok::Ty(ty) = next.tok() {
                        file.next();
                        if let Some(next) = file.next() {
                            if let Tok::Expr(Expr::Ident(i)) = next.tok() {
                                vars.push((ty, i));
                            }
                        }
                    }
                }
                debug!("parse_g: Done parsing struct types");
                let g = G::GStruct(ident, vars);
                return Ok(g);
            }
            Tok::Expr(Expr::Ident(_)) => {
                // GFuncDef of custom type
                file.nerr()?.assert_tok(Tok::Binop(Binop::Star))?;
                info!("parse_g: Found helper function");
            }
            e => {
                error!("Tried to parse a fn or global but did not find any type or var");
                return Err(AppError::new(
                    1,
                    format!("{:#?} was not a expected identifier", e),
                ));
            }
        };
    }
    Err(AppError::new(
        1,
        format!("Could not match found kword, next element was None."),
    ))
}

fn parse_func(file: &mut File, ty: Ty, ident: String) -> Result<GFuncDef, AppError> {
    // We have already found T,r in parese_g.
    // Now, we might be a GFuncDecl or a GFuncDef,
    // Depending on if we find a scope.
    if let Some(next) = file.next() {
        match next.tok() {
            Tok::Delim(Delim::LPar) => {
                // Parse out all parameters
                trace!("Parsing parameters from file");
                let params = parse_func_params(file)?;
                info!("Parsed parameters: {:?}", params);

                //Check next token for left curly
                if let Some(next) = file.next() {
                    if next.equals(Tok::Delim(Delim::LCurl)) {
                        debug!("Parsing function scope of {} {}", ty, ident);
                        let scope = parse_scope(file)?;
                        let func = GFuncDef::new(ty, ident, params, scope);
                        return Ok(func);
                    } else {
                        return Err(AppError::new(
                            0,
                            format!("Expect Left curl after right parent:{:?}", file.peek()),
                        ));
                    }
                }

                Err(AppError::new(
                    1,
                    format!("Failed to parse function. Last token:{:?}", next),
                ))
            }
            e => Err(AppError::new(
                0,
                format!("TODO: parse ass and fn, got {:?}", e),
            )),
        }
    } else {
        Err(AppError::new(
            1,
            format!("Could not find more tokens while expected"),
        ))
    }
}

fn parse_statement(file: &mut File) -> Result<S, AppError> {
    match file.nerr()?.tok() {
        Tok::Kword(Kword::Delete) => {
            file.nerr()?.assert_tok(Tok::Delim(Delim::LBrack))?;
            file.nerr()?.assert_tok(Tok::Delim(Delim::RBrack))?;
            if let Tok::Expr(Expr::Ident(id)) = file.nerr()?.tok() {
                file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                return Ok(S::SDelete(id));
            }
        }
        Tok::Kword(Kword::Break) => {
            file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
            return Ok(S::SBreak);
        }
        Tok::Kword(Kword::For) => {
            debug!("parse_statement: handling for loop");
            file.nerr()?.assert_tok(Tok::Delim(Delim::LPar))?;

            let mut outer_s: Vec<Box<S>> = Vec::new();
            let fordecl = parse_statement(file)?;
            debug!("parse_statement: for: Decleration part: {:?}", fordecl);
            outer_s.push(Box::new(fordecl));

            let cond = parse_expr(file, 0)?;
            file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
            debug!("parse_statement: for: Condition part: {:?}", cond);

            let inc = parse_statement(file)?;
            debug!("parse_statement: for: incremental part: {:?}", inc);

            trace!(
                "parse_statement: for: next token: {:?}",
                file.nperr()?.tok()
            );
            let s: Box<S>;
            if let Tok::Delim(Delim::LCurl) = file.nperr()?.tok() {
                trace!("parse_statement: for: inner scope is scope");
                file.next();
                s = Box::new(parse_scope(file)?);
            } else {
                trace!("parse_statement: for: inner scope is statement");
                s = Box::new(parse_statement(file)?);
                debug!("parse_statement: for: Parsed single statement, not a scope");
            }
            debug!("parse_statement: FOR's inner scope: {:?}", s);
            if let Some(cond) = cond {
                let mut inner_s: Vec<Box<S>> = Vec::new();
                inner_s.push(s);
                inner_s.push(Box::new(inc));
                let s = Box::new(S::SScope(inner_s));

                let while_s = S::SWhile(cond, s);
                outer_s.push(Box::new(while_s));

                return Ok(S::SScope(outer_s));
            }
        }
        Tok::Kword(Kword::While) => {
            let e = parse_expr(file, 0)?.unwrap();
            let s: Box<S>;
            if let Some(n) = file.peek().clone() {
                if let Tok::Delim(Delim::LBrack) = n.tok() {
                    file.next();
                    s = Box::new(parse_scope(file)?);
                } else {
                    s = Box::new(parse_statement(file)?);
                }
                debug!("parse_statement: while's type: {:?}", s);
                let scop = S::SWhile(e, s);
                return Ok(scop);
            }
        }
        Tok::Kword(Kword::If) => {
            trace!("parse_statement: if: next: {:?}", file.peek());
            // SIf(E, S, Option<S>)

            // need to pop the (
            file.next();

            // the if condition
            let e = parse_expr(file, 0)?;
            if e.is_none() {
                return Err(AppError::new(1, format!("no expression for when to if")));
            }
            let e = e.unwrap();
            file.nerr()?.assert_tok(Tok::Delim(Delim::RPar))?;
            trace!("parse_statement: if e: {:?}", e);
            debug!("parse_statement: if's next tok: {:?}", file.peek());

            // IF then this
            let s: Box<S>;
            if let Some(_) = file.peek().clone() {
                s = Box::new(parse_statement(file)?);
                trace!("parse_statement: then s: {:?}", s);
                let s2: Option<Box<S>>;
                trace!(
                    "parse_statement: Is there a else after if? {:?}",
                    file.peek()
                );
                if Tok::Kword(Kword::Else) == file.nperr()?.tok() {
                    trace!("parse_statement: parsing else s2");
                    //
                    // There was a else statement.
                    // Parse it and return
                    file.next();
                    s2 = Some(Box::new(parse_statement(file)?));
                    let scop = S::SIf(e, s, s2);
                    return Ok(scop);
                }
                let scop = S::SIf(e, s, None);
                return Ok(scop);
            }
        }
        Tok::Kword(Kword::Return) => {
            // NOTE: Returns... return. no need to parse more scope
            trace!("parse_statement: entered keword::return handling");
            let mut e = None;
            if let Tok::Delim(Delim::SemiCol) = file.nperr()?.tok() {
            } else {
                e = parse_expr(file, 0)?; // should not pop semi, but pops if only semi
            }
            file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
            info!("parse_statement: Parsed epxression: {:?}", e);
            let returned = S::SReturn(e);
            return Ok(returned);
        }
        Tok::Delim(Delim::LCurl) => {
            trace!("parse_scope: entered LCurl handling");
            // The scope has ended.
            // Make sure the right curl is popped since it was only inspected until here.
            let inner = parse_scope(file)?;
            return Ok(inner);
        }
        Tok::Expr(Expr::Ident(ident)) => {
            trace!("parse_statement: entered identifier handling");
            // Just a identifier is not enough to know what to do, need next token to check for
            // assignsments, function calls etc.
            // Then, once we know what we need, request it
            // Note: Could be a SVarAssign(r, e)
            // This is part of the expression handler
            //
            // SvarAssign(r,e)
            // SArrayAssign(r,e,Option(r), e)
            // SExpr(r)

            match file.nerr()?.tok() {
                Tok::Binop(Binop::Star) => {
                    debug!("parse_scope: Looks like a TPoint");
                    let mut name = String::new();

                    if let Some(next) = file.next() {
                        if let Tok::Expr(Expr::Ident(id)) = next.tok() {
                            name = id;
                        } else {
                            let mut err =
                                AppError::new(1, format!("Not use how to handle ident* _"));
                            err.line = Some(next.line());
                            return Err(err);
                        }
                    }
                    if let Some(n) = file.next() {
                        n.assert_tok(Tok::Delim(Delim::Eq))?;
                    }
                    let expr = parse_expr(file, 0)?;
                    if let Some(expr) = expr {
                        let point = Ty::TPoint(Box::new(Ty::TIdent(ident)));
                        let s = S::SVarDef(point, name, expr);
                        return Ok(s);
                    } else {
                        let mut err = AppError::new(
                            1,
                            format!("TPoint should be followed by expression, not optionally!"),
                        );
                        // unwrapping here is bad and I feel bad.
                        err.line = Some(file.peek().clone().unwrap().line());
                        return Err(err);
                    }
                }
                Tok::Delim(Delim::LBrack) => {
                    // Parse array access.
                    // ex:
                    // a1[2].x = 3;
                    // a1[2] = 5;
                    //
                    // a1[2].x = 3;
                    // into
                    // SArrayAssign("a1", EInt(2), "x", EInt(3))
                    //
                    //
                    // Parse the inner brackets
                    let e1 = parse_expr(file, 0)?;
                    debug!("parse_statement: parsign array asign: {:?}", e1);
                    //
                    // Get the following token
                    let next = file.nerr()?;
                    trace!("parse_statement: Next: {:?}", next);

                    // See what follows the arrary access
                    match next.tok() {
                        Tok::Delim(Delim::Dot) => {
                            // for the optional r
                            if let Some(e1) = e1 {
                                // This gives us the values we are looking for wrapped in a S.
                                let struvar = file.nerr()?;
                                if let Tok::Expr(Expr::Ident(struvar)) = struvar.tok() {
                                    match file.nerr()?.tok() {
                                        // we might have a =, but also ++ or --
                                        Tok::Delim(Delim::Eq) => {
                                            let eq = parse_expr(file, 0)?;
                                            if let Some(eq) = eq {
                                                return Ok(S::SArrayAssign(
                                                    ident,
                                                    e1,
                                                    Some(struvar),
                                                    eq,
                                                ));
                                            }
                                        }
                                        Tok::Binop(binop) => {
                                            let e = Box::new(E::EArrayAccess(
                                                ident.clone(),
                                                Box::new(e1.clone()),
                                                Some(struvar.clone()),
                                            ));
                                            if let Binop::DMin = binop {
                                                let e = E::EBinOp(
                                                    Binop::Minus,
                                                    e,
                                                    Box::new(E::EInt(1)),
                                                );
                                                file.nerr()?
                                                    .assert_tok(Tok::Delim(Delim::SemiCol))?;
                                                return Ok(S::SArrayAssign(
                                                    ident,
                                                    e1,
                                                    Some(struvar),
                                                    e,
                                                ));
                                            } else if let Binop::DPlus = binop {
                                                let e =
                                                    E::EBinOp(Binop::Plus, e, Box::new(E::EInt(1)));
                                                file.nerr()?
                                                    .assert_tok(Tok::Delim(Delim::SemiCol))?;
                                                return Ok(S::SArrayAssign(
                                                    ident,
                                                    e1,
                                                    Some(struvar),
                                                    e,
                                                ));
                                            }
                                        }
                                        e => {
                                            let err = AppError::new(1, format!("parse_statement: unsure how to access array by {:?}", e));
                                            return Err(err);
                                        }
                                    }
                                }
                            }
                        }
                        Tok::Delim(Delim::Eq) => {
                            // for direct access
                            if let Some(e1) = e1 {
                                if let Some(e2) = parse_expr(file, 0)? {
                                    let s = S::SArrayAssign(ident, e1, None, e2);
                                    return Ok(s);
                                }
                            }
                        }
                        Tok::Binop(binop) => {
                            if let Some(e1) = e1 {
                                let e = Box::new(E::EArrayAccess(
                                    ident.clone(),
                                    Box::new(e1.clone()),
                                    None,
                                ));
                                if let Binop::DMin = binop {
                                    let e = E::EBinOp(Binop::Minus, e, Box::new(E::EInt(1)));
                                    file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                                    return Ok(S::SArrayAssign(ident, e1, None, e));
                                } else if let Binop::DPlus = binop {
                                    let e = E::EBinOp(Binop::Plus, e, Box::new(E::EInt(1)));
                                    file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                                    return Ok(S::SArrayAssign(ident, e1, None, e));
                                }
                            }
                            error!("parse_statement: parsed array access but did not return it!");
                        }
                        e => {
                            let mut err = AppError::new(
                                1,
                                format!(
                                    "parse_statement: unexpected {:?} while parseing array access",
                                    e
                                ),
                            );
                            err.line = Some(next.line());
                            return Err(err);
                        }
                    }
                }
                Tok::Delim(Delim::Eq) => {
                    debug!("parse_scope: Found decleration, {:?} = ...", ident);
                    if let Some(ex) = parse_expr(file, 0)? {
                        file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                        info!("Scope: Parsed Expression: {:?}", ex);
                        return Ok(S::SVarAssign(ident, ex));
                    }
                }
                Tok::Delim(Delim::LPar) => {
                    // putchar('0' + x / i);
                    trace!("parse_scope: function call by LPar match");
                    let params = parse_call_params(file)?; // Should NOT pop Semi

                    // ;
                    debug!("parse_scope: call_params: {:?}", params);
                    let mut lhs = E::ECall(ident, params);
                    let e = precidence_climb(file, 0, &mut lhs)?;
                    // ;
                    debug!("parse_scope: further lineparse: {:?}", e);
                    file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                    return Ok(S::SExpr(e));
                }
                Tok::Binop(Binop::DPlus) => {
                    let e = E::EBinOp(
                        Binop::Plus,
                        Box::new(E::EVar(ident.clone())),
                        Box::new(E::EInt(1)),
                    );
                    match file.nperr()?.tok() {
                        Tok::Delim(Delim::SemiCol) => {
                            debug!("parse_statement: Semi after Double bin");
                            file.next();
                        }
                        Tok::Delim(Delim::RPar) => {
                            debug!("parse_statement: RPar after Double bin");
                            file.next();
                        }
                        other => {
                            debug!(
                                "parse_statement: other token after Double operator: {:?}",
                                other
                            );
                            return Err(AppError::new(
                                1,
                                format!("No allowed follower to DMin after ident"),
                            ));
                        }
                    }
                    return Ok(S::SVarAssign(ident, e));
                }
                Tok::Binop(Binop::DMin) => {
                    let e = E::EBinOp(
                        Binop::Minus,
                        Box::new(E::EVar(ident.clone())),
                        Box::new(E::EInt(1)),
                    );
                    match file.nperr()?.tok() {
                        Tok::Delim(Delim::SemiCol) => {
                            debug!("parse_statement: Semi after Double bin");
                            file.next();
                        }
                        Tok::Delim(Delim::RPar) => {
                            debug!("parse_statement: RPar after Double bin");
                            file.next();
                        }
                        other => {
                            debug!(
                                "parse_statement: other token after Double operator: {:?}",
                                other
                            );
                            return Err(AppError::new(
                                1,
                                format!("No allowed follower to DMin after ident"),
                            ));
                        }
                    }
                    return Ok(S::SVarAssign(ident, e));
                }
                Tok::Delim(Delim::SemiCol) => {
                    let e = S::SExpr(E::EVar(ident));
                    return Ok(e);
                }
                n => {
                    return Err(AppError::new(
                        1,
                        format!(
                            "
                                parse_statement: unknown follower after identifier({}): {:?}",
                            ident, n
                        ),
                    ))
                }
            }
        }
        Tok::Ty(ty) => {
            trace!("parse_scope: entered type({}) handling", ty);
            // We get a type. This means something is beinng assigned
            //  -> SVarDef(T,r,e)
            //  where
            //  T: Ty
            //  r: ident
            //  e: Typewrapper around value.
            //

            // Note: We have already parsed out the type, so we skip it
            // Note: This code is ugly af, might refactor it later
            if let Some(next) = file.next() {
                if let Tok::Expr(Expr::Ident(ident)) = next.tok() {
                    if let Some(next) = file.next() {
                        if let Tok::Delim(Delim::Eq) = next.tok() {
                            let pres = parse_expr(file, 0)?;
                            if let Some(e) = pres {
                                file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                                return Ok(S::SVarDef(ty, ident, e));
                            }
                        } else {
                            let mut err = AppError::new(
                                1,
                                format!("Type + ident should be followed by =, instead {:?}", next),
                            );
                            err.line = Some(next.line());
                            return Err(err);
                        }
                    }
                } else if let Tok::Binop(Binop::Star) = next.tok() {
                    let ty = Ty::TPoint(Box::new(ty));
                    let next = file.nerr()?;
                    if let Tok::Expr(Expr::Ident(ident)) = next.tok() {
                        let next = file.nerr()?;
                        if let Tok::Delim(Delim::Eq) = next.tok() {
                            let pres = parse_expr(file, 0)?;
                            if let Some(e) = pres {
                                // pop semi
                                file.nerr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                                return Ok(S::SVarDef(ty, ident, e));
                            }
                        }
                    }
                } else {
                    let mut err = AppError::new(
                        1,
                        format!(
                            "Types in scope are followed by identifiers, instead found: {:?}",
                            next
                        ),
                    );
                    err.line = Some(next.line());
                    return Err(err);
                }
            }
        }
        r => {
            return Err(AppError::new(
                1,
                format!("parse_statement: Encountered unknown token {:?}", r),
            ))
        }
    }
    let err = AppError::new(
        1,
        format!("parse_statement: Did not return parse_statement"),
    );
    Err(err)
}

/// Parses out a scope. Expects the last symbol to be a Right curly bracket. Should not pop
/// semicolons itself, but rather let that to parse_statements which it is using internally.
fn parse_scope(file: &mut File) -> Result<S, AppError> {
    // This parses out s, the scope.
    // s is defined in cigrid rules 39-41.
    //
    // s contains a log of keyword structs and assignsments.
    // We are looking into expressions and seeing how they evaluate
    // (See the parse_expr function).
    //
    //
    let mut res: Vec<Box<S>> = Vec::new();

    trace!("parse_scope: Entered");

    while let Some(next) = file.peek().clone() {
        // if right parent, return here!
        trace!("parse_scope: handling {:?} ", next);
        if let Tok::Delim(Delim::RCurl) = next.tok() {
            file.next();
            warn!("parse_scope: Done parsing scope: {:?}", res);
            return Ok(S::SScope(res));
        }
        res.push(Box::new(parse_statement(file)?));
    }
    warn!("Parsed outside of scope parser");
    Err(AppError::new(1, format!("no closing bracket")))
}

/// Here we have a function call we need to parse the parameters for. It should not pop any
/// potential semicolon following the call.
/// Note: () should be allowed to enter here, as in empty args.
/// Note: '(' is consumed before entering here
///
fn parse_call_params(file: &mut File) -> Result<Vec<Box<E>>, AppError> {
    let mut res: Vec<Box<E>> = Vec::new();

    // catch the case of no arguments
    if let Tok::Delim(Delim::RPar) = file.nperr()?.tok() {
        file.next();
        return Ok(res);
    }

    debug!("Parse-Call-Params entered.");
    loop {
        trace!(
            "parse_call_params: {:?} as part of function params",
            file.peek()
        );

        // parse expression
        let e = parse_expr(file, 0)?;
        trace!("parse_call_params: Parameter: {:?}", e);

        if let Some(e) = e {
            res.push(Box::new(e));
        } else {
            warn!("parse_call_params: failed to parse more params.");
            info!("parse_call_params: next peek: {:?}", file.nperr()?);
            return Ok(res);
        }
        // ONLY contue if the next tok is a comma, otherwise you are done!
        if let Tok::Delim(Delim::Comma) = file.nperr()?.tok() {
            file.next();
            continue;
        } else if let Tok::Delim(Delim::RPar) = file.nperr()?.tok() {
            file.next();
            break;
        } else {
            return Err(AppError::new(
                1,
                format!("parse_call_params: Uknown next: {:?}", file.nperr()?),
            ));
        }
    }
    info!("parse_call_params: Parsed params: {:?}", res);
    Ok(res)
}

fn precidence_climb(file: &mut File, prec: usize, lhs: &mut E) -> Result<E, AppError> {
    trace!("precidence_climb: (start) got ({})", lhs);
    let mut rhs: Option<E>; // Always initially empty
    let mut op: Binop;
    let mut lah: Binop;

    if let Some(token) = file.peek() {
        trace!("precidence_climb: (start) lah candidate: {:?}", token);
        if let Tok::Binop(token) = token.tok() {
            lah = token;
        } else {
            warn!("precidense_climb: Next is not a Binop, returning lhs");
            return Ok((*lhs).clone());
        }
    } else {
        warn!("precidense_climb: No more tokens while setting look a head");
        return Ok((*lhs).clone());
    }

    'o: while lah.prec() > prec {
        debug!(
            "precidence_climb: (outer) lahprec vs prec: ({} > {})",
            lah.prec(),
            prec
        );
        if let Some(peeked) = file.peek().clone() {
            trace!(
                "precidence_climb: (outer)peeked next operator in outer loop start for binop: {:?}",
                peeked.tok()
            );
            if let Tok::Binop(_) = peeked.tok() {
            } else {
                trace!(
                    "precidence_climb: (outer) it was not binop, it was {:?}",
                    peeked.tok()
                );
                break 'o;
            }
        }

        op = lah;
        file.next();
        rhs = parse_expr(file, lah.prec())?;
        if rhs.is_none() {
            return Err(AppError::new(
                1,
                format!("precidence_climb: Did not get a right hand side for unknown reason..."),
            ));
        }
        if let Some(peeked) = file.peek().clone() {
            trace!(
                "precidence_climb: peeked next operator in outer loop: {:?}",
                peeked.tok()
            );
            if let Tok::Binop(nextop) = peeked.tok() {
                lah = nextop;
            }
        }

        trace!("precidence_climb: got rhs from parse_expr: {:?}", rhs);
        debug!(
            "precidence_climb: checking inner conditions ({:?} , {:?})",
            lah.prec(),
            op.prec()
        );

        'i: while lah.prec() > op.prec() || lah.prec() == op.prec() && !lah.lassoc() {
            debug!(
                "precidence_climb: (inner) looping inner  ({} {} > {} {})",
                lah,
                lah.prec(),
                op.prec(),
                op
            );
            // get right hand side.
            // We have already made sure right hand side is a Some(), unwrapping is fiiiiiine
            rhs = Some(precidence_climb(file, lah.prec() - 1, &mut rhs.unwrap())?);
            debug!("precidence_climb: (inner) loop got rhs: {:?}", rhs);

            // Update lookahead
            if let Some(token) = file.peek() {
                trace!(
                    "precidence_climb: (inner) peeked next operator in inner loop: {:?}",
                    token.tok()
                );
                if let Tok::Binop(potential_next) = token.tok() {
                    info!(
                        "precidense_climb: (inner) next is binop: {:?}",
                        potential_next
                    );
                    lah = potential_next;
                } else {
                    warn!("precidense_climb: (inner) Next is not a Binop, aborting inner loop");
                    break 'i;
                }
            }
            trace!("precidence_climb: (inner) updated lah: {:?}", lah);
        }

        *lhs = E::EBinOp(op, Box::new(lhs.clone()), Box::new(rhs.unwrap()));
        info!("precidence climb: parsed {:#?}", lhs);
        trace!("precidence climb:   lah:{:?}  op:{:?}", lah, op);
    }
    debug!("precidence_climb: (end) returning: {:?}", lhs);
    Ok((*lhs).clone())
}

/// Parses a e::= Expression as defined by the cigrid abstract syntax 36-38.
///
/// Should be called with (file, 0, None) as default. Note about this function is that it should
/// not pop the semicolon as the semi is part of statement, not expr!
fn parse_expr(file: &mut File, prec: usize) -> Result<Option<E>, AppError> {
    // We get the lefthand side of what we have parsed
    // as a complete E. This should ofcourse be added to the right hand side of any expression.
    // Crux: This can be empty, indicatind the start of a expression, which must ofcourse be
    // handled _slightly_  seperatly.
    debug!(
        "parse_expr: Entered (prec:{}). First token: {:?}",
        prec,
        file.peek()
    );
    match file.nerr()?.tok() {
        Tok::Expr(exp) => {
            // There might be more parts connected by operators
            if let Some(peeked) = file.peek().clone() {
                debug!(
                    "parse_expr: found expr {:?}, matching token: {:?} to see what to do next.",
                    exp, peeked
                );
                match peeked.tok() {
                    Tok::Delim(Delim::LPar) => {
                        match exp {
                            Expr::Ident(i) => {
                                // This looks like a function call!
                                file.next(); // popp the (
                                let args = parse_call_params(file)?;
                                trace!("parse_expr: parsed function call params: {:?}", args);
                                return Ok(Some(E::ECall(i, args)));
                            }
                            _ => {
                                // Not sure what this is , but parsing the rest as expression.
                                return Ok(Some(E::from_expr(exp)));
                            }
                        }
                    }
                    Tok::Delim(Delim::LBrack) => {
                        trace!("parse_expr: Handlling array. Next: {:?}", file.nperr()?);
                        // we can easily happen to need to perform array access.
                        file.next(); // Popp the LBrack
                        let array_index = parse_expr(file, prec)?;
                        debug!("parse_expr: Array index: {:?}", array_index);
                        debug!("parse_expr: next tok: {:?}", file.nperr()?);
                        // We should have returned on the right bracket, so there shoudl be
                        // more data either in the form of a semi or a further expression that
                        // can be parsed int o e through a precidence climb.

                        if let Expr::Ident(ident) = exp {
                            if let Some(array_index) = array_index {
                                let mut lhs = E::EArrayAccess(
                                    ident.clone(),
                                    Box::new(array_index.clone()),
                                    None,
                                );
                                match file.nperr()?.tok() {
                                    Tok::Delim(Delim::Dot) => {
                                        file.nerr()?;
                                        let name = file.nerr()?;
                                        if let Tok::Expr(Expr::Ident(name)) = name.tok() {
                                            lhs = E::EArrayAccess(
                                                ident,
                                                Box::new(array_index),
                                                Some(name),
                                            );
                                            let res = precidence_climb(file, prec, &mut lhs)?;
                                            return Ok(Some(res));
                                        }
                                    }
                                    Tok::Delim(Delim::SemiCol) => {
                                        debug!("parse_expr: array access was followed by semi, returning: {:?}", lhs);
                                        return Ok(Some(lhs));
                                    }
                                    other => {
                                        info!("parse_expr: Array access was not followed by dot: {:?}", other);
                                        let res = precidence_climb(file, prec, &mut lhs)?;
                                        return Ok(Some(res));
                                    }
                                }
                                return Ok(Some(lhs));
                            }
                        } else {
                            let err = AppError::new(
                                1,
                                format!(
                                    "parse_expr: could not parse expression as identifier: {:?}",
                                    exp
                                ),
                            );
                            return Err(err);
                        }
                    }
                    Tok::Delim(Delim::SemiCol)
                    | Tok::Delim(Delim::Comma)
                    | Tok::Delim(Delim::RPar) => {
                        trace!("parse_expr: Found {:?} after expr", peeked.tok());
                        return Ok(Some(E::from_expr(exp)));
                    }
                    Tok::Delim(Delim::RBrack) => {
                        file.next();
                        trace!("parse_expr: Found {:?} after expr", peeked.tok());
                        return Ok(Some(E::from_expr(exp)));
                    }
                    Tok::Binop(e) => {
                        // current token: ident
                        // e := lookahead, not consumed yet.

                        trace!(
                            "parse_expr: Found expr {:?} followed by binop: {:?} ",
                            exp,
                            e
                        );
                        let mut lhs = E::from_expr(exp); // left hand side in prec climb
                        let res = precidence_climb(file, prec, &mut lhs)?;
                        trace!("parse_expr: done precidence climbing: {:?}", res);
                        debug!("parse_expr: returning: {:?}", res);
                        return Ok(Some(res));
                    }
                    r => {
                        return Err(AppError::new(
                            1,
                            format!("parse_expr: Encountered invalid token after expr {:?}", r),
                        ))
                    }
                }
            }
            return Err(AppError::new(
                1,
                format!("parse_expr: unfished loop handling expression"),
            ));
        }
        Tok::Delim(Delim::LPar) => {
            // New expr, set prec to 0 as we are parsing new expr.
            debug!("parse_expr: Found LPar, parsing insides as expression");
            let e = parse_expr(file, 0)?;
            if let Some(e) = e.clone() {
                if let Tok::Binop(_) = file.nperr()?.tok() {
                    // we can have a expression such as
                    // (5) + 1
                    // So we need to check for binary operators before returning.
                    let res = precidence_climb(file, prec, &mut e.clone())?;
                    file.nerr()?.assert_tok(Tok::Delim(Delim::RPar))?;
                    info!("parse_expr: Parsed parenthasised expression: {:?}", res);
                    return Ok(Some(res));
                }
            }
            file.nerr()?.assert_tok(Tok::Delim(Delim::RPar))?;
            info!("parse_expr: Parsed parenthasised expression: {:?}", e);
            return Ok(e);
        }
        Tok::Kword(Kword::New) => {
            let mut ty: String = String::new();
            if let Some(next) = file.next() {
                if let Tok::Expr(Expr::Ident(t)) = next.tok() {
                    ty = t;
                }
            }
            // Handle [int];
            if let Some(lbr) = file.next() {
                lbr.assert_tok(Tok::Delim(Delim::LBrack))?;
                if let Some(size) = file.next() {
                    if let Tok::Expr(Expr::Int(i)) = size.tok() {
                        let e = E::ENew(Ty::TIdent(ty), Box::new(E::EInt(i)));
                        if let Some(rbr) = file.next() {
                            rbr.assert_tok(Tok::Delim(Delim::RBrack))?;
                        }
                        file.nperr()?.assert_tok(Tok::Delim(Delim::SemiCol))?;
                        trace!("parse_expr: parsed new: {:?}", e);
                        return Ok(Some(e));
                    } else if let Tok::Expr(Expr::Ident(i)) = size.tok() {
                        let e = E::ENew(Ty::TIdent(ty), Box::new(E::EVar(i)));
                        if let Some(rbr) = file.next() {
                            rbr.assert_tok(Tok::Delim(Delim::RBrack))?;
                        }
                        trace!("parse_expr: parsed new: {:?}", e);
                        return Ok(Some(e));
                    }
                }
            }
        }
        Tok::Delim(Delim::SemiCol) => {
            return Err(AppError::new(
                1,
                format!("parse_expr: tried to handle semi!"),
            ));
        }
        Tok::Delim(Delim::RPar) | Tok::Delim(Delim::RBrack) => {
            return Ok(None);
        }
        Tok::Unop(unop) => {
            let exp = parse_expr(file, prec)?.unwrap();
            let inner = E::EUnOp(unop, Box::new(exp));
            return Ok(Some(inner));
        }
        other => {
            return Err(AppError::new(
                1,
                format!("parse_expr: Encountered unhandled token {:?}", other),
            ))
        }
    }
    error!(
        "Should not leave parse_expr if_let. Next token: {:#?}",
        file.peek()
    );
    Err(AppError::new(
        1,
        format!("Somehow left parse_expr without returning correctly."),
    ))
}

/// Parses function def parameters, such as (int a, int b)
/// Restriction: the (  LPar must be popped
fn parse_func_params(file: &mut File) -> Result<Params, AppError> {
    let mut pars: Vec<(Ty, String)> = Vec::new();
    while let Some(a) = file.next() {
        if let Tok::Ty(ty) = a.tok() {
            if let Some(next) = file.next() {
                if let Tok::Expr(Expr::Ident(name)) = next.tok() {
                    pars.push((ty, name));
                } else if let Tok::Binop(Binop::Star) = next.tok() {
                    let next = file.nerr()?;
                    if let Tok::Expr(Expr::Ident(name)) = next.tok() {
                        pars.push((ty, name));
                    }
                }
            }
        } else if let Tok::Delim(Delim::Comma) = a.tok() {
            if !pars.is_empty() {
                continue;
            } else {
                let mut err = AppError::new(1, "cannot start oparamaters with comma".to_string());
                err.line = Some(a.line());
                return Err(err);
            }
        } else if let Tok::Delim(Delim::RPar) = a.tok() {
            debug!("Parsed params: {:#?}", pars);
            let params = Params::new(pars);
            return Ok(params);
        } else {
            break;
        }
    }
    debug!("Parsed params: {:#?}", pars);
    Err(AppError::new(
        1,
        format!(
            "could not parse parameters! In function pars_params. Remaining file: {:#?}",
            file
        ),
    ))
}

#[cfg(test)]
mod tests {
    #[test]
    fn init() {
        let _ = env_logger::builder()
            .is_test(true)
            .filter_level(log::LevelFilter::Trace)
            .try_init();
    }

    use crate::errors::AppError;
    use crate::scanner::scan;
    use std::io::Write;
    use tempfile::NamedTempFile;

    use super::*;

    fn clean(s: String) -> String {
        s.replace(" ", "").replace("\n", "")
    }

    #[test]
    fn arith_assignments_s() -> Result<(), AppError> {
        let answer = String::from(
            "GFuncDef(TVoid, \"arith_assignments\", {(TInt,\"x\") (TInt,\"y\") (TInt,\"z\")},
            SScope({
                SVarAssign(\"x\", EBinOp(+, EVar(\"x\"), EInt(1)))
                    SVarAssign(\"x\", EBinOp(-, EVar(\"x\"), EBinOp(*, EVar(\"y\"), EInt(8))))
                    SVarAssign(\"y\", EBinOp(/, EBinOp(%, EInt(12), EVar(\"z\")), EInt(3)))
            }))",
        );
        let file = "examples/arith_assignments.cpp";
        let mut scanned = scan(file)?;
        match parse(&mut scanned) {
            Ok(c) => {
                let c = format!("{}", c);
                assert_eq!(clean(c), clean(answer));
                Ok(())
            }
            Err(e) => {
                println!("Encountered errro: {:?}", e);
                Err(e)
            }
        }
    }
    #[test]
    fn invalid_curlcombo() -> Result<(), AppError> {
        let file = "examples/invalid_curls.cpp";
        let mut scanned = scan(file)?;

        match parse(&mut scanned) {
            Ok(c) => {
                panic!("Should error, instead got: {}", c)
            }
            Err(_) => {}
        }
        Ok(())
    }
    #[test]
    fn invalid_naked_minmin() -> Result<(), AppError> {
        let file = "examples/naked_--.cpp";
        let mut scanned = scan(file)?;

        match parse(&mut scanned) {
            Ok(c) => {
                panic!("Should error, instead got: {}", c)
            }
            Err(_) => {}
        }
        Ok(())
    }
    #[test]
    fn invalid_return_binop() -> Result<(), AppError> {
        let file = "examples/invalid_return_binop.cpp";
        let mut scanned = scan(file)?;

        match parse(&mut scanned) {
            Ok(c) => {
                panic!("Should error, instead got: {}", c)
            }
            Err(_) => {}
        }
        Ok(())
    }
    #[test]
    fn naked_return_return() -> Result<(), AppError> {
        let ansfile = "examples/naked_return_return.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/naked_return_return.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn naked_return() -> Result<(), AppError> {
        let ansfile = "examples/naked_return.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/naked_return.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn complete_s() -> Result<(), AppError> {
        let ansfile = "examples/complete_s.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/complete_s.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn extern_func_decl() -> Result<(), AppError> {
        let ansfile = "examples/extern_func_decl.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/extern_func_decl.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn parentier_hell() -> Result<(), AppError> {
        let ansfile = "examples/parentier_hell.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/parentier_hell.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn parent_hell() -> Result<(), AppError> {
        let ansfile = "examples/parent_hell.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/parent_hell.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    #[ignore]
    fn node() -> Result<(), AppError> {
        let ansfile = "examples/node.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/node.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn logic_operations() -> Result<(), AppError> {
        let ansfile = "examples/logic_operations.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/logic_operations.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn call_two_args_var_expr() -> Result<(), AppError> {
        let ansfile = "examples/call_two_args_var_expr.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/call_two_args_var_expr.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn print_int() -> Result<(), AppError> {
        let ansfile = "examples/print_int.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/print_int.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    #[ignore]
    fn struct_arrays() -> Result<(), AppError> {
        let ansfile = "examples/struct_arrays.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/struct_arrays.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn defining_local_variables() -> Result<(), AppError> {
        let ansfile = "examples/defining_local_variables.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/defining_local_variables.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn for_loop_with_break() -> Result<(), AppError> {
        let ansfile = "examples/for_loop_with_break.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/for_loop_with_break.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    #[ignore]
    fn complete_example() -> Result<(), AppError> {
        let ansfile = "examples/examples.txt";
        if let Ok(answer) = std::fs::read_to_string(ansfile) {
            let file = "examples/examples.cpp";
            let mut scanned = scan(file)?;

            match parse(&mut scanned) {
                Ok(c) => {
                    let c = format!("{}", c);
                    assert_eq!(clean(c), clean(answer));
                }
                Err(e) => panic!("{}", e),
            }
        }
        Ok(())
    }
    #[test]
    fn if_then_dangling_else() -> Result<(), AppError> {
        let answer = String::from(
            "
            GFuncDef(TInt, \"if_then_dangeling_else\", {(TInt,\"x\")},
            SScope({
                SIf(EBinOp(>, EVar(\"x\"), EInt(0)),
                SIf(EBinOp(<, EVar(\"x\"), EInt(10)),
                SReturn(EBinOp(+, EVar(\"x\"), EInt(1))),
                SReturn(EBinOp(+, EVar(\"x\"), EInt(2)))), )
                    SReturn(EVar(\"x\"))
            })) ",
        );
        let mut file = NamedTempFile::new().unwrap();
        write!(file, "int if_then_dangeling_else(int x){{ if(x > 0) if(x < 10) return x + 1; else return x + 2; return x; }} ").unwrap();
        let mut scanned = scan(file.path().to_str().unwrap())?;

        match parse(&mut scanned) {
            Ok(c) => {
                let c = format!("{}", c);
                assert_eq!(clean(c), clean(answer));
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    #[test]
    fn function_call_one_arg() -> Result<(), AppError> {
        let answer = String::from(
            " GFuncDef(TInt, \"call_one_args\", {}, SScope({ SReturn(ECall(\"one_param_return\",{EInt(123)})) }))");
        let mut file = NamedTempFile::new().unwrap();
        write!(file, "// Function call with one argument (S)\n int call_one_args(){{\n return one_param_return(123);\n }}").unwrap();
        let mut scanned = scan(file.path().to_str().unwrap())?;

        match parse(&mut scanned) {
            Ok(c) => {
                let c = format!("{}", c);
                assert_eq!(clean(c), clean(answer));
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    #[test]
    fn return_escapee() -> Result<(), AppError> {
        let answer = String::from("GFuncDef(TVoid, \"foo\", {}, SScope({SReturn(EChar('\\\"'))}))");
        let file = "examples/escaped_slash.cpp";
        let mut scanned = scan(file)?;
        match parse(&mut scanned) {
            Ok(c) => {
                let c = format!("{}", c);
                assert_eq!(clean(c), clean(answer));
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    #[test]
    fn return_char_lit() -> Result<(), AppError> {
        let answer = String::from("GFuncDef(TVoid, \"foo\", {}, SScope({SReturn(EChar('a'))}))");
        let file = "examples/char_literal.cpp";
        let mut scanned = scan(file)?;
        match parse(&mut scanned) {
            Ok(c) => {
                let c = format!("{}", c);
                assert_eq!(clean(c), clean(answer));
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    #[test]
    fn call_no_args() -> Result<(), AppError> {
        let answer = String::from(
            "GFuncDef(TVoid, \"call_no_args_call_statemant\", {}, SScope({ SExpr(ECall(\"empty\",{}))}))");
        let file = "examples/call_no_args.cpp";
        let mut scanned = scan(file)?;
        match parse(&mut scanned) {
            Ok(c) => {
                let c = format!("{}", c);
                assert_eq!(clean(c), clean(answer));
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    #[test]
    fn parse_one_param_returned() -> Result<(), AppError> {
        let answer =
            "GFuncDef(TInt,\"one_param_return\",{(TInt,\"x\")},SScope({SReturn(EVar(\"x\"))}))";
        let mut file = NamedTempFile::new().unwrap();
        write!(file, "int one_param_return(int x){{return x;}}").unwrap();
        let mut scanned = scan(file.path().to_str().unwrap())?;
        match parse(&mut scanned) {
            Ok(c) => {
                assert_eq!(format!("{}", c).replace(" ", "").replace("\n", ""), answer);
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    #[test]
    fn parse_empty_file() -> Result<(), AppError> {
        let mut file = NamedTempFile::new().unwrap();
        write!(file, "").unwrap();
        let mut scanned = scan(file.path().to_str().unwrap())?;

        match parse(&mut scanned) {
            Ok(c) => panic!(
                "parse should fail on empty files with exitcode 0\nInstead we got: {:#?}",
                c
            ),
            Err(e) => assert_eq!(e.code, 0),
        }
        Ok(())
    }
    #[test]
    fn parse_empty_function() -> Result<(), AppError> {
        let answer = "GFuncDef(TVoid,\"empty\",{},SScope({}))";
        let filename = String::from("examples/empty_function.cpp");
        let mut scanned = scan(&*filename)?;
        let parsed = parse(&mut scanned)?;
        assert_eq!(
            format!("{}", parsed).replace(" ", "").replace("\n", ""),
            answer
        );
        Ok(())
    }
}
