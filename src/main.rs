#[macro_use]
extern crate log;
use cigrid::errors;
use cigrid::run;

use clap::{App, Arg};

// This is only the runner
// With clap, parsing input is trivial
fn main() {
    env_logger::init();

    let matches = App::new("Cilly Cigrid Compiler")
        .version("0.1")
        .author("Aron Hansen Berggren, aronber@kth.se")
        .about("compiles cigrid programs")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("pp")
                .short("v")
                .long("--pretty-print")
                .multiple(false)
                .help("Prints the AST after the parser is done"),
        )
        .arg(
            Arg::with_name("ir")
                .short("i")
                .long("--ir")
                .multiple(false)
                .help("Prints the IR code. This is the representation before the assembler is generated after the AST."),
        )
        .arg(
            Arg::with_name("asm")
                .short("a")
                .long("--asm")
                .multiple(false)
                .help("Prints the final generated asm."),
        )
        .arg(
            Arg::with_name("asmir")
                .long("--asmir")
                .multiple(false)
                .help("Prints the generated intermediate asm."),
        )
        .get_matches();
    debug!("{:#?}", matches);
    let _ = match run(matches) {
        Ok(_) => Ok(()),
        Err(error) => {
            errors::handle(error);
            Err(())
        }
    };
}
