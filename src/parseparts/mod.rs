use std::fmt;

use self::e::E;
use self::s::S;

use crate::tokens::ty::Ty;

pub mod e;
pub mod s;

/// A Global, which contains some file-wide defenitions
///
/// These are all listed in the abstract Cigrid syntax under
/// rule 42-43
///
#[derive(Debug, Clone)]
pub enum G {
    GFuncDef(GFuncDef),
    GVarDec(GVarDec),
    GVarDef(GVarDef),
    GFuncDecl(Ty, String, Params),
    GStruct(String, Vec<(Ty, String)>),
}

impl fmt::Display for G {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        trace!("Printing Global: {:?}", self);
        match self {
            G::GFuncDef(r) => write!(f, "{}", r),
            G::GVarDec(r) => write!(f, "{}", r),
            G::GVarDef(r) => write!(f, "{}", r),
            G::GStruct(ident, v) => {
                write!(f, "GStruct({}, {{", ident)?;
                for (ty, name) in v {
                    write!(f, "({}, {})", ty, name)?;
                }
                write!(f, "}}")
            }
            G::GFuncDecl(t, r, params) => {
                write!(f, "GFuncDecl({}, \"{}\", {{{}}})", t, r, params)
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct GVarDec {
    /// GVarDecl
    ty: Ty,
    ident: String,
}

impl GVarDec {
    pub fn new(ty: Ty, ident: String) -> Self {
        Self { ty, ident }
    }
}
impl fmt::Display for GVarDec {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "GVarDec{}", self)
    }
}

#[derive(Debug, Clone)]
pub struct GVarDef {
    /// GVarDecl
    ty: Ty,
    ident: String,
    val: E,
}

impl GVarDef {
    pub fn new(ty: Ty, ident: String, val: E) -> Self {
        Self { ty, ident, val }
    }
}

impl fmt::Display for GVarDef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "GVarDef{}", self)
    }
}

#[derive(Debug, Clone)]
pub struct GFuncDef {
    /// GFuncDef
    pub ty: Ty,
    pub ident: String,
    pub params: Params,
    pub scope: S,
}

impl GFuncDef {
    pub fn new(ty: Ty, ident: String, params: Params, scope: S) -> Self {
        Self {
            ty,
            ident,
            params,
            scope,
        }
    }
}

impl fmt::Display for GFuncDef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        trace!("Printing Func: {:?}", self);
        write!(
            f,
            "GFuncDef({}, \"{}\", {{{}}},\n {})",
            self.ty, self.ident, self.params, self.scope
        )
    }
}

#[derive(Debug, Clone)]
pub struct Params {
    /// A vector over T,r
    /// where T is a type
    /// and r is some identifier
    pub pars: Vec<(Ty, String)>,
}

impl Params {
    pub fn new(pars: Vec<(Ty, String)>) -> Self {
        Self { pars }
    }
}
impl fmt::Display for Params {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        trace!("Printing parameters: {:?}", self);
        let st: String = self
            .pars
            .iter()
            .map(|(ty, s)| format!("({},\"{}\")", ty, s))
            .collect();
        write!(f, "{}", st)
    }
}
