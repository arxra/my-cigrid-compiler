use std::fmt;

use crate::tokens::ty::Ty;

use super::e::E;

#[derive(Debug, Clone)]
pub enum S {
    SReturn(Option<E>),
    SExpr(E),
    SVarDef(Ty, String, E),
    SVarAssign(String, E),
    SScope(Vec<Box<S>>),
    SIf(E, Box<S>, Option<Box<S>>),
    SArrayAssign(String, E, Option<String>, E),
    SWhile(E, Box<S>),
    SBreak,
    SDelete(String),
}

impl fmt::Display for S {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let st = match self {
            Self::SArrayAssign(r, e, or, e2) => {
                if let Some(r2) = or {
                    format!("SArrayAssign(\"{}\", {}, \"{}\", {})", r, e, r2, e2)
                } else {
                    todo!("Not able to print SArrayAssign without dots");
                    //format!("SArrayAssign(\"{}\"), {}, \"{}\", {}", r, e, r, e2)
                }
            }
            Self::SReturn(e) => {
                if let Some(e) = e {
                    format!("SReturn({})", e)
                } else {
                    format!("SReturn()")
                }
            }

            Self::SExpr(e) => format!("SExpr({})", e),
            Self::SVarAssign(ident, e) => format!("SVarAssign(\"{}\", {})", ident, e),
            Self::SIf(r, s, s2) => {
                if let Some(s2) = s2 {
                    format!("SIf({}, {}, {})", r, s, s2)
                } else {
                    format!("SIf({}, {}, )", r, s)
                }
            }
            Self::SVarDef(ty, r, e) => format!("SVarDef({}, \"{}\", {})", ty, r, e),
            Self::SScope(s) => {
                let mut string = String::from("SScope({");
                for st in s {
                    string.push_str(&*format!("{}\n", st))
                }
                string.push_str("})");

                format!("{}", string)
            }
            Self::SWhile(e, s) => format!("SWhile({},{})", e, s),
            Self::SBreak => String::from("SBreak"),
            Self::SDelete(r) => format!("SDelete(\"{}\")", r),
        };
        trace!("Printing scope: {:?}", st);
        writeln!(f, "{}", st)
    }
}
