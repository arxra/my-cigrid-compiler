use std::fmt;

use crate::tokens::binop::Binop;
use crate::tokens::expr::Expr;
use crate::tokens::ty::Ty;
use crate::tokens::unop::Unop;

/// Handling of expression rules, 36-38
#[derive(Debug, Clone)]
pub enum E {
    EVar(String),
    EInt(isize),
    EChar(char),
    EString(String),
    EBinOp(Binop, Box<E>, Box<E>),
    EUnOp(Unop, Box<E>),
    ECall(String, Vec<Box<E>>), // Function call
    ENew(Ty, Box<E>),
    EArrayAccess(String, Box<E>, Option<String>),
}

impl E {
    pub fn from_expr(exp: Expr) -> Self {
        match exp {
            Expr::Ident(i) => Self::EVar(i),
            Expr::Int(i) => Self::EInt(i),
            Expr::Char(c) => Self::EChar(c),
            Expr::String(s) => Self::EString(s),
            Expr::BInt(u, i) => Self::EUnOp(u, Box::new(Self::EInt(i))),
        }
    }
}

impl fmt::Display for E {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let st = match self {
            E::EVar(e) => format!("EVar(\"{}\")", e),
            E::EInt(i) => format!("EInt({})", i),
            E::EChar(i) => format!("EChar('{}')", i.escape_default()),
            E::EString(i) => format!("EString({})", i),
            E::EBinOp(bop, e, e2) => format!("EBinOp({}, {}, {})", bop, e, e2),
            E::EUnOp(unop, i) => format!("EUnOp({}, {})", unop, i),
            E::ECall(s, v) => {
                let mut st = format!("ECall(\"{}\",{{", s);
                for e in v {
                    st.push_str(&*format!("{}", *e));
                }
                st.push_str("})");
                st
            }
            E::ENew(t, e) => format!("ENew({}, {})", t, e),
            E::EArrayAccess(r, e, or) => {
                if let Some(or) = or {
                    format!("EArrayAccess(\"{}\", {}, \"{}\")", r, e, or)
                } else {
                    format!("EArrayAccess(\"{}\", {}, )", r, e)
                }
            }
        };
        write!(f, "{}", st)
    }
}
