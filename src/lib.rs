use clap::ArgMatches;
use errors::AppError;

#[macro_use]
extern crate log;

mod asm;
mod asm_ir;
pub mod errors;
mod hybrid_ir;
mod p_file;
mod parseparts;
mod parser;
mod scanner;
mod tokens;

pub fn compile(input: String) -> Result<String, AppError> {
    let scanned = scanner::scan_str(input)?;
    flow(scanned)
}

// This is only the runner
pub fn run(opts: ArgMatches) -> Result<(), AppError> {
    let filename = opts.value_of("INPUT").unwrap();
    let scanned = scanner::scan(filename)?;
    debug!("Scanned: \n{:#?}", scanned);
    println!("{}", flow(scanned)?);
    Ok(())
}

fn flow(mut file: scanner::file::File) -> Result<String, AppError> {
    let parsed = parser::parse(&mut file)?;
    let hybir = hybrid_ir::IRProg::new(parsed)?;
    let mut asmir = asm_ir::AsmIR::new(hybir)?;
    let asm = asm::Asm::new(&mut asmir)?;
    Ok(format!("{}", asm))
}
