use std::collections::HashMap;
use std::fmt;

use crate::{
    errors::AppError,
    hybrid_ir::{Blockend, IRFunc, IRProg, IRStmt, Sym},
    parseparts::e::E,
    tokens::binop::Binop,
    tokens::ty::Ty,
};

#[derive(Debug, Clone, Default)]
pub struct Env {
    pub names: HashMap<String, Register>,
    pub id: usize,
    pub totbuf: isize,
    ftempt: Vec<Register>,
}

impl Env {
    pub fn new() -> Self {
        let mut names = HashMap::new();
        let sym = Sym::new(String::from("return_reg"));
        let ret = Register::new(sym.clone(), Some(8), Some(WSize::Qword));
        names.insert(sym.sym, ret);

        Self {
            names,
            id: 1,
            totbuf: 8,
            ftempt: Vec::new(),
        }
    }

    pub fn insert(&mut self, sym: Sym, oty: Option<Ty>) -> Register {
        //! Given a symbol this keeps track of the assigned registers for our variables.
        let size = Self::insert_space(self, oty.clone());
        let id = self.nid();
        let sy = Sym::new(format!("{}_{}", sym, id));

        // Create a new register, the offset in memory is the current limit which will be expanded
        // below for housing this value. The size is a known WSize.
        let reg = Register::new(sy, Some(size), WSize::from_ty(oty));
        self.names.insert(sym.sym, reg.clone());
        reg
    }

    pub fn count(&self) -> usize {
        //! Returns the number of registers env has given out, intended for use when allocating
        //! spillable memory.
        self.names.len()
    }

    fn ret_tmp(&mut self, reg: Register) {
        self.ftempt.push(reg)
    }

    fn insert_space(&mut self, ty: Option<Ty>) -> isize {
        //! Creates space for ty and returns the offset from where to reach a ty.
        //! Assumes no more space than ty is used, so you would get memory corruption if a 64 bit
        //! value is used after given 32 from this function.
        let mut size = None;
        if let Some(ty) = ty {
            size = match ty {
                Ty::Void => Some(WSize::Zero),
                Ty::Int => Some(WSize::Qword),
                Ty::Char => Some(WSize::Word),
                Ty::TIdent(_) => Some(WSize::Qword),
                Ty::TPoint(_) => Some(WSize::Qword),
            };
        }
        self.totbuf += WSize::convert_to_bytes(size);
        self.totbuf
    }

    pub fn get_tmp(&mut self) -> Register {
        //! Returns a temporary name that is available.
        //! It is up to you to return it to the env, which should try to give out as few
        //! temporaries as possible.
        //! Note: This should also be in memory, not on the stack. So make room for the temporaries
        //! as well, as they will also be reversed back to adresses.
        //! For simplicity, always 64 bits.
        match self.ftempt.pop() {
            Some(reg) => reg,
            None => {
                let sym = Sym {
                    sym: String::from("tmp"),
                };
                self.insert(sym, None)
            }
        }
    }

    pub fn get_reg(&self, sym: &Sym) -> Result<Register, AppError> {
        //! Given a symbol, either returns the register of the symbol or a error. The error would
        //! arrise if you try to use a variable which have not been decleared already.
        match self.names.get(&sym.sym) {
            Some(e) => Ok(e.clone()),
            None => Err(AppError::new(
                1,
                format!("Could not find sym ({}) in env: \n{}", sym, self),
            )),
        }
    }
    pub fn nid(&mut self) -> usize {
        self.id += 1;
        self.id
    }
}

impl fmt::Display for Env {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "=========== env =========")?;
        writeln!(f, "count: {}", self.count())?;
        for name in self.names.clone() {
            writeln!(f, "\t{:?}", name)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum WSize {
    Qword,
    Dword,
    Word,
    Zero,
}

impl WSize {
    fn from_ty(ty: Option<Ty>) -> Option<Self> {
        if let Some(ty) = ty {
            match ty {
                Ty::Void => Some(WSize::Zero),
                Ty::Int => Some(WSize::Qword),
                Ty::Char => Some(WSize::Word),
                Ty::TIdent(_) => Some(WSize::Qword),
                Ty::TPoint(_) => Some(WSize::Qword),
            }
        } else {
            Some(WSize::Qword)
        }
    }
    fn convert_to_bytes(size: Option<Self>) -> isize {
        //! Takes a optinal WSize and returns the number of bytes needed to store that size.
        //! Usefull for calculating offsets given a number of registers.
        if let Some(size) = size {
            match size {
                WSize::Qword => 8,
                WSize::Dword => 4,
                WSize::Word => 2,
                WSize::Zero => 0,
            }
        } else {
            8
        }
    }
}

impl fmt::Display for WSize {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Qword => {
                write!(f, "qword")
            }
            Self::Dword => {
                write!(f, "dword")
            }
            Self::Word => {
                write!(f, "word")
            }
            Self::Zero => {
                write!(f, "none")
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Register {
    sym: Sym,
    offset: Option<isize>,
    size: Option<WSize>,
}

impl Register {
    pub fn new(sym: Sym, offset: Option<isize>, size: Option<WSize>) -> Self {
        Self { sym, offset, size }
    }
}

impl fmt::Display for Register {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.size.is_some() {
            write!(f, "{} ", self.size.clone().unwrap())?;
        }
        if self.offset.is_some() {
            // A offset of Some(0) is a derefrence!
            if self.offset.unwrap() == 0 {
                write!(f, "[rbp]")?;
            } else {
                write!(f, "[rbp-{}]", self.offset.unwrap())?;
            }
        } else {
            write!(f, "{}", self.sym)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub enum Kword {
    Extern,
    Global,
    Section,
    Call,
}

impl fmt::Display for Kword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Kword::Extern => {
                write!(f, "extern")
            }
            Kword::Global => {
                write!(f, "global")
            }
            Kword::Section => {
                write!(f, "section")
            }
            Kword::Call => {
                write!(f, "call")
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum Inst {
    Pop,
    Mov,
    Sub,
    Add,
    Ret,
    Mul,
    Push,
}

impl Inst {
    pub fn from_binop(bin: Binop) -> Self {
        match bin {
            Binop::Plus => Self::Add,
            Binop::Minus => Self::Sub,
            Binop::FSlash => Self::Mul,
            unknown_op => {
                todo!("Binop to instruction: handle {}", unknown_op)
            }
        }
    }
}

impl fmt::Display for Inst {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Inst::Mov => {
                write!(f, "mov")
            }
            Inst::Sub => {
                write!(f, "sub")
            }
            Inst::Add => {
                write!(f, "add")
            }
            Inst::Ret => {
                write!(f, "ret")
            }
            Inst::Mul => {
                write!(f, "mul")
            }
            Inst::Pop => {
                write!(f, "pop")
            }
            Inst::Push => {
                write!(f, "push")
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum Asmline {
    Assign(Register, isize),
    Inst(Inst, Option<Register>, Option<Register>),
    Label(String),
    Decl(Kword, String),
}

impl fmt::Display for Asmline {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Inst(inst, r1, r2) => {
                if let (Some(r1), Some(r2)) = (r1, r2) {
                    writeln!(f, "\t{}\t{},\t{}", inst, r1, r2)
                } else if let Some(r1) = r1 {
                    writeln!(f, "\t{}\t{}", inst, r1)
                } else {
                    writeln!(f, "\t{}", inst)
                }
            }
            Self::Label(l) => {
                writeln!(f, "{}:", l)
            }
            Self::Decl(k, s) => {
                writeln!(f, "\t{}\t{}", k, s)
            }
            Self::Assign(sym, val) => {
                writeln!(f, "\tmov\t{},\t{}", sym, val)
            }
        }
    }
}

/// Assembler Intermediate Representation.
/// This struct holds a list of almost complete assembler lines,
/// which are still missing some register allocations and other things such as register preservations as well as a environment full off information on how to resolve these last things in order to make this assembler compileable.
#[derive(Debug, Clone)]
pub struct AsmIR {
    pub insts: Vec<Asmline>,
    pub env: Env,
}

impl AsmIR {
    pub fn new(prog: IRProg) -> Result<Self, AppError> {
        trace!("Creating asmir from IRProg: \n{}", prog);
        let mut env = Env::new();
        let mut res = Vec::new();
        let mut restprog: Vec<IRFunc> = Vec::new();

        // First, consume all extern
        for hir in prog.funcs {
            if hir.blocks.is_empty() {
                // If the block is empty but it's a function, its a extern.
                res.push(Asmline::Decl(Kword::Extern, hir.sym.sym));
            } else {
                restprog.push(hir);
            }
        }

        res.push(Asmline::Decl(Kword::Global, String::from("main")));
        res.push(Asmline::Decl(Kword::Section, String::from(".text")));

        for hir in restprog {
            info!("asmir_new: parsing hir: {}", hir);
            // These hirs all have blocks, so parse them carefully.
            // For S, this is a single block.
            for block in hir.blocks {
                debug!("asmir_new: Handling block: {}", block);
                // push the block label to get it out of the way.
                res.push(Asmline::Label(block.sym.sym));
                for line in block.stms {
                    debug!("asmir_new: parsing lines in statement: {}", line);
                    // Parse each line of IRS to AsmIR
                    // This is the instruction selection, and known to be... hard.
                    // Work with the env, and use temporary registers.
                    // This is still just another IR, not the ASM
                    // Uhhhhhhrm
                    //
                    // This can only be one of three different IRS's though, so just handle those
                    // three and you'll be fiiiiiiiineeeeee (for S level).
                    match line {
                        IRStmt::IRSExpr(expr) => {
                            // shorthand for I don't care anymore, give this function a temp
                            // registry just so I do not need to rewrite expr_res to handle
                            // Option<reg>
                            //
                            let reg = env.get_tmp();
                            let mut resxpr = Self::expr_res(&mut env, reg.clone(), expr)?;
                            res.append(&mut resxpr);
                            env.ret_tmp(reg)
                        }
                        IRStmt::IRSVarAssign(sym, expr) => {
                            // assigning a expression to a symbol requires expression resolution
                            // first, and the possible use of temporary variables to resolve it.
                            let reg = env.get_reg(&sym)?;
                            let mut expr_res = Self::expr_res(&mut env, reg, expr)?;
                            res.append(&mut expr_res);
                        }
                        IRStmt::IRSVarDecl(sym, ty) => {
                            // Declaring a new value is nothing more than a reservation of the
                            // symbol name in the environment of variables.
                            let resreg = env.insert(sym.clone(), Some(ty));
                            trace!("inserted {} into env and got {:?}", sym, resreg);
                        }
                    }
                }
                // For the endblock, parse out the Option<Expr> and make sure the right thing is
                // returned if applicable.
                let retreg = Register::new(Sym::new("rax".to_string()), None, None);
                match block.end {
                    Blockend::IRSReturn(oexpr) => {
                        if let Some(expr) = oexpr {
                            let mut returnexpr = Self::expr_res(&mut env, retreg, expr)?;
                            res.append(&mut returnexpr);
                        } else {
                            res.push(Asmline::Assign(retreg.clone(), 0));
                        }
                        res.push(Asmline::Inst(Inst::Ret, None, None));
                    }
                    other => {
                        todo!("Not supported blockend: {:?}", other)
                    }
                }
            }
        }

        info!("Cleaning AsmIR");
        let mut insts = Vec::new();
        let r10 = Register::new(Sym::new(String::from("r10")), None, None);
        for line in res.clone() {
            // We have some cleaning to do:
            // mov x <- x               ; Does not need to happen
            // mov QWORD[], QWORD[]     ; Does not work. Use intermediate registry
            match line {
                Asmline::Inst(Inst::Mov, Some(r1), Some(r2)) if r1 == r2 => {
                    // This optimization has precedence over the fix below.
                    debug!(
                        "tried moving same registor to self, skipping insert: Asmline: mov {}, {})",
                        r1, r2
                    )
                }
                Asmline::Inst(inst, Some(r1), Some(r2))
                    if r1.offset.is_some() && r2.offset.is_some() =>
                {
                    // If we have 2 following offsets, transfer the value through a intermediate
                    insts.push(Asmline::Inst(Inst::Mov, Some(r10.clone()), Some(r2)));
                    insts.push(Asmline::Inst(inst, Some(r1), Some(r10.clone())));
                }
                other => insts.push(other),
            }
        }

        Ok(Self { insts, env })
    }
    fn expr_res(env: &mut Env, reg: Register, expr: E) -> Result<Vec<Asmline>, AppError> {
        let mut res: Vec<Asmline> = Vec::new();
        debug!("expr_res: Trying to resolve {} to reg: {}", expr, reg);
        match expr {
            E::EVar(sym) => {
                // This variable must already be declared, so it exists in env.
                let resline =
                    Asmline::Inst(Inst::Mov, Some(reg), Some(env.get_reg(&Sym::new(sym))?));
                res.push(resline);
            }
            E::EInt(int) => {
                // just return the result as written to a register
                res.push(Asmline::Assign(reg, int));
            }
            E::EChar(cha) => {
                // just return the result as written to a register
                let c = cha as isize;
                info!("CHAR to ASCII: {} -> {}", cha, c);
                res.push(Asmline::Assign(reg, c));
            }
            E::EString(stri) => {
                // just return the result as written to a register, and perhaps data
                todo!("process E::String: {}", stri);
            }
            E::EBinOp(binop, ex1, ex2) => {
                // parse ex1
                // parse ex2
                // perform binop on ex1, ex2
                // Do not forget to give back temp variables here!
                //
                res.append(&mut Self::expr_res(env, reg.clone(), *ex1)?);

                let tmp_reg = env.get_tmp();
                res.append(&mut Self::expr_res(env, tmp_reg.clone(), *ex2)?);

                let inst = Inst::from_binop(binop);
                let resline = Asmline::Inst(inst, Some(reg), Some(tmp_reg.clone()));
                res.push(resline);

                // returns the temp1 and 2, marking them as available space since we are done
                // computing with the values they held
                env.ret_tmp(tmp_reg);
            }
            E::EUnOp(uno, ex1) => {
                //
                todo!("E::EUnOp not handles: {}, {}", uno, ex1);
            }
            E::ECall(fun, args) => {
                for (arg, reg) in args.iter().zip(Self::get_return_list().iter()) {
                    // This is really cool, we are creating a touple from a known vec and unknown
                    // vec. Thanks to that zip returns None whenever either returns none, this
                    // function matches the args perfectly.
                    // https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.zip
                    //
                    let mut resline = Self::expr_res(env, reg.clone(), *arg.clone())?;
                    res.append(&mut resline);
                }
                let resline = Asmline::Decl(Kword::Call, fun);
                res.push(resline);
            }
            e => {
                todo!("Not supported action: cannot parse {} to asm_ir yet!", e)
            }
        }
        Ok(res)
    }

    fn get_return_list() -> [Register; 6] {
        [
            Register::new(Sym::new(String::from("rdi")), None, None),
            Register::new(Sym::new(String::from("rsi")), None, None),
            Register::new(Sym::new(String::from("rdx")), None, None),
            Register::new(Sym::new(String::from("rcx")), None, None),
            Register::new(Sym::new(String::from("r8")), None, None),
            Register::new(Sym::new(String::from("r9")), None, None),
        ]
    }
}

impl fmt::Display for AsmIR {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for air in self.insts.clone() {
            write!(f, "{}", air)?;
        }
        debug!("{}", self.env);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::{parseparts::e::E, parser::parse, scanner::scan};

    use super::*;

    #[test]
    fn assign_int() -> Result<(), AppError> {
        let mut env = Env::new();
        let testint = 5;
        let expr = E::EInt(testint);
        let tmpreg = env.get_tmp();

        let lines = AsmIR::expr_res(&mut env, tmpreg, expr)?;
        println!("{:?}", lines);
        if let Asmline::Assign(_, _testint) = lines.last().unwrap() {
            Ok(())
        } else {
            panic!();
        }
    }
    #[test]
    fn simple() -> Result<(), AppError> {
        let mut scanned = scan("examples/ir_simp.cpp")?;
        let parsed = parse(&mut scanned)?;
        let hir = IRProg::new(parsed)?;
        let _asmir = AsmIR::new(hir)?;
        Ok(())
        //Err(AppError::new(1, "".to_string()))
        //
    }
}
