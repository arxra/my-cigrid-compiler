use std::fmt;

// Custom error type; can be any type which defined in the current crate
pub struct AppError {
    pub code: i32,
    pub message: String,
    pub line: Option<usize>,
}

impl AppError {
    pub fn new(code: i32, message: String) -> AppError {
        AppError {
            code,
            message,
            line: None,
        }
    }
    pub fn notok(line: usize) -> AppError {
        let mut e = Self::new(1, format!("Was expecting more tokens, but ran out"));
        e.line = Some(line);
        e
    }
}

// Implement std::fmt::Display for AppError
impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(line) = self.line {
            write!(f, "{}", line)
        } else {
            write!(f, "{}", &self.message)
        }
    }
}

// Implement std::fmt::Debug for AppError
impl fmt::Debug for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{{ file: {}, line: {}, message: {} }}",
            file!(),
            line!(),
            &self.message
        ) // programmer-facing output
    }
}

pub fn handle(e: AppError) {
    // Don't print to stdout here!
    error!("{}: CODE: {}", e.message, e.code);
    std::process::exit(e.code);
}
