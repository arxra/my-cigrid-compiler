use crate::errors::AppError;
use crate::scanner::file::*;

pub mod file;

/// This is also commonly refered to as the lexer.
///
/// Seperates text hopefully as we need it.
/// Almost just a wrapper around File::new
/// In particular, we need to seperate the text into tokens which can be parsed.
/// Scan takes the raw string value of what needs to be parsed.
pub fn scan(filename: &str) -> Result<File, AppError> {
    let filestr = read_file(filename)?;
    debug!("filestr: {:#?}", filestr);
    let file = File::new(filestr, String::from(filename))?;
    info!("file: {:#?}", file);
    Ok(file)
}

pub fn scan_str(file: String) -> Result<File, AppError>{
    let file = File::new(file, String::from("nofile"))?;
    Ok(file)
}

fn read_file(filename: &str) -> Result<String, AppError> {
    match std::fs::read_to_string(filename) {
        Ok(content) => {
            debug!(
                "Read file {} as \n=========\n{}\n========",
                filename, content
            );
            Ok(content)
        }
        Err(_) => Err(AppError::new(1, format!("No such file {}", filename))),
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::errors::AppError;
    use crate::tokens::Token;

    #[test]
    fn invalid_integer_return() -> Result<(), AppError> {
        let file = "examples/invalid_integer_return.cpp";
        match scan(file) {
            Ok(c) => {
                panic!("Should error, instead got: {}", c)
            }
            Err(_) => {}
        }
        Ok(())
    }

    #[test]
    fn insert_space() -> Result<(), AppError> {
        let testline = "Tree* t = node(node(leaf(1), 2, leaf(3)), 4, leaf(5));";
        let expected =
            "Tree * t = node ( node ( leaf ( 1 ) , 2 , leaf ( 3 ) ) , 4 , leaf ( 5 ) ) ;";
        let ans = File::insert_space(testline)?;

        assert_eq!(ans, expected);
        Ok(())
    }

    #[test]
    fn cigrid_scan() -> Result<(), AppError> {
        scan("./examples/examples.cpp")?;
        Ok(())
    }

    #[test]
    fn scan_int() -> Result<(), AppError> {
        let testfile = String::from("./examples/int.test");
        assert_eq!(read_file(&*testfile).unwrap(), "int\n");
        if let Ok(file) = scan(&*testfile) {
            if let Ok(tok) = Token::new("int", testfile.clone(), 1, 0) {
                println!("tok : {:#?}", tok);
                println!("file: {:#?}", file.file);
                assert!(file.contains(&tok));
            }
        }
        Ok(())
    }

    #[test]
    fn scan_aith_ass_ext() -> Result<(), AppError> {
        let filename = String::from("examples/aith_ass_ext.cpp");
        //let expans = String::from("// Empty function ( S ) \n void empty ( ) { \n } \n");
        let ans_r = scan(&*filename);
        match ans_r {
            Ok(ans) => {
                println!("file: {:#?}", ans);
                assert_eq!(ans.file.len(), 21); // If this fails, check for -8 vs - 8
                Ok(())
            }
            Err(e) => {
                println!("ERROR:\n{:#?}", e);
                panic!();
            }
        }
    }

    #[test]
    fn scan_simple_function() -> Result<(), AppError> {
        let filename = String::from("examples/empty_function.cpp");
        //let expans = String::from("// Empty function ( S ) \n void empty ( ) { \n } \n");
        let ans_r = scan(&*filename);
        match ans_r {
            Ok(mut ans) => {
                println!("file: {:#?}", ans);
                assert_eq!(ans.file.len(), 5); // If this fails, we probably don't exclude comments
                assert_eq!(ans.next().unwrap(), Token::new("void", filename, 11, 0)?); // If this fails, we are not popping the first element
                Ok(())
            }
            Err(e) => {
                println!("ERROR:\n{:#?}", e);
                panic!();
            }
        }
    }
}
