use crate::errors::AppError;
use crate::tokens::Token;
use std::fmt;

#[derive(Debug, Clone)]
pub struct File {
    next: Option<Token>,
    pub file: Vec<Token>,
}

impl File {
    pub fn len(&self) -> usize {
        self.file.len()
    }
    pub fn is_empty(&self) -> bool {
        self.file.is_empty()
    }
    pub fn peek(&self) -> &Option<Token> {
        &self.next
    }
    /// Either returns a clone of the peekable next, or errors if no such element exists.
    /// This means it is safe to hold this value while sending the File along.
    pub fn nperr(&mut self) -> Result<Token, AppError>{
        if let Some(n) = self.next.clone() {
            return Ok(n)
        }
        let err = AppError::new(1, format!("No more tokens! Could not peek next!"));
        Err(err)
    }
    pub fn nerr(&mut self) -> Result<Token, AppError>{
        let last = self.next.clone();
        self.next = self.file.pop();
        trace!(
            "file: Forwarded the file, next({:?}): returned({:?})",
            self.next.clone(),
            last
        );
        if let Some(last) = last{
            Ok(last)
        } else {
            let err = AppError::new(1, format!("No more tokens! more was expected!"));
            Err(err)
        }

    }
    pub fn next(&mut self) -> Option<Token> {
        let last = self.next.clone();
        self.next = self.file.pop();
        trace!(
            "Forwarded the file, next({:?}): returned({:?})",
            self.next.clone(),
            last
        );
        last
    }
    pub fn contains(&self, other: &Token) -> bool {
        if let Some(e) = &self.next {
            e.eq(other)
        } else {
            (&self.file).contains(other)
        }
    }
    pub fn insert_space(line: &str) -> Result<String, AppError> {
        let mut nline: String = String::new();
        let mut iter = line.chars();

        let mut last = String::new();

        let acs = ['>', '<', '=', '!', '&', '|', '-', '+'];

        info!("scanning iterator:\n{:#?}", iter);

        'main: while let Some(c) = iter.next() {
            debug!("insert_space: handling ({}), last: {:?}", c, last);
            // When to actually inser space? some symbols such as < vs << makes this hard. Some
            // sort of allow if in list?
            // Only skip the first occurance of these tokens (when inserting spaces),
            // the second one should be skipped.

            if c == '\'' {
                // Speciall case to handle apostrothees
                if !nline.ends_with(' ') {
                    nline.push(' ');
                }
                nline.push(c);
                let c = iter.next();
                if let Some(c) = c {
                    nline.push(c);
                } else {
                    return Err(AppError::new(
                        1,
                        format!("Tried scanning a char literal but did not find literal."),
                    ));
                }
                if c == Some('\\') {
                    // if it's escaped, then advance char once
                    if let Some(c) = iter.next(){
                        nline.push(c);
                    }
                }
                if let Some(b) = iter.next() {
                    if b == '\'' {
                        nline.push(b);
                    } else {
                        return Err(AppError::new(
                                1,
                                format!(
                                    "Tried scanning a char literal ({:?}) but it was not matched.",
                                    nline.chars().nth_back(0).unwrap()
                                ),
                        ));
                    }
                }
                nline.push(' ');
                continue;
            } else if (!c.is_ascii_digit()) 
                && (!c.is_ascii_alphabetic()) 
                && ((!acs.contains(&c))
                    || !last.is_empty())
                && (c != ' ')
                && (c != '_') // Can be part of varname
                && (c != ';')
                && (c != '\"')
                /*"*/ // Save the higlight!
                && !nline.ends_with(' ')
            {
                info!("insert_space: Extra space needed!");
                // Last coherent variable is over
                nline.push(' ');
                if !last.is_empty() {
                    last.pop();
                }
            } else if c == '\"'
            /*"*/ // Save the higlight!
            // Lets see if we cannot find ourselves a string.
            // Don't insert spaces into string, we need them intact!
            { if let Some(prev) = nline.chars().last(){
                    if prev != ' '{
                        nline.push(' ');
                    }
                }
                info!("insert_space: Going through string!");
                nline.push(c);
                let mut l = c;
                while let Some(c) = iter.next() {
                    if c == '"' && l != '\\' {
                        // No, this is failure
                        debug!("insert_space: DONE: scanning c({}) with old l({})", c, l);
                        nline.push(c);
                        nline.push(' ');
                        continue 'main;
                    } else {
                        trace!("insert_space: Scanning c({}) with old l({})", c, l);
                        nline.push(c);
                        l = c;
                    }
                }
                error!(
                    "FAILURE: ran out of elements while scanning c({}) with old l({})",
                    c, l
                );
                info!("insert_space: Handled string.");
            } else if c == ';' {
                if !(nline.chars().last() == Some(' ')) {
                    nline.push(' ');
                }
                nline.push(c);
                continue;
            }

            #[allow(clippy::clippy::if_same_then_else)]
            if !(nline.ends_with(' ') && c == ' ') {
                if let Some(prev) = nline.chars().last(){
                    if ((c.is_ascii_digit() && prev != '-') || c.is_ascii_alphabetic()) && last.is_empty() && prev != ' ' {
                        nline.push(' ');
                    } else if acs.contains(&c) && !acs.contains(&prev) && prev !=' ' {
                        nline.push(' ');
                    } else {
                        last.pop();
                    }
                }
                else if c == ';' {
                    // IF the line had more information to parse, then insert space before next item.
                    nline.push(' ');
                }

                // Push the elment
                nline.push(c);

                //Check if space might be needed before next round
                if (c.is_ascii_digit() || c.is_ascii_alphabetic() || c == '_') && last.is_empty() {
                    last.push('1');
                }

            }
        }
        if nline.ends_with(' ') {
            nline.pop();
        }
        Ok(nline)
    }
    fn comment_strip(raw: String) -> Result<String, AppError> {
        // TODO: MultiLine comments should ofc not be ignored.
        let mut mlc = String::new(); // set this to true while in a multiline comment.
        let mut res = String::new();
        'f: for line in raw.lines() {
            debug!("Checking for comments (mlc: {}): {}", mlc, line);
            let mut liter = line.chars();
            'q: while let Some(c) = liter.next() {
                if (c == '/' || c == '#') && mlc.is_empty() {
                    if let Some(d) = liter.next() {
                        if (d == 'i' || d == '/') && mlc.is_empty() {
                            res.push('\n');
                            continue 'f;
                        } else if d == '*' {
                            info!("Multiline comment being removed");
                            mlc.push('1');
                        } else if mlc.is_empty() {
                            if c != '\n' {
                                res.push(c);
                            }
                            if d != '\n' {
                                res.push(d);
                            }
                        }
                        else{
                            res.push(c);
                        }
                    } else {
                        res.push(c);
                    }
                } else if mlc.is_empty() && c != '\n'{
                    res.push(c);
                    continue 'q;
                } else if c !='\n' { // mlc must have contents here
                    if c == '*' {
                        let next = liter.next();
                        if let Some('/') = next {
                            info!("Multiline comment finished");
                            mlc.pop();
                            continue 'q;
                        }
                    }
                continue 'q;
                }
            }
            res.push('\n');
        }
        if !mlc.is_empty(){
            return Err(AppError::new(1, format!("Not closed multiline comment!")));
        }
        info!("Removed comments: {}", res);
        debug!("found lines: {}", res.lines().count());
        Ok(res)
    }

    pub fn new(s: String, filename: String) -> Result<File, AppError> {
        let mut f = File {
            file: Vec::new(),
            next: None,
        };
        info!("New file input s: \n{:#?}", s);
        let mut tcount: usize = 0;

        let s = Self::comment_strip(s)?;
        info!("Removed comments: \n{:#?}", s);

        for (i, line) in s.lines().enumerate() {
            info!("line: {:?}:{}", line, i + 1);

            let nline: String = Self::insert_space(line)?;
            info!("nline {:?}: {:?}", nline, i + 1);

            for word in nline.split_whitespace() {
                info!("parsing word on line {}: {:?}", i + 1, word);

                let tok = Token::new(word, filename.clone(), i + 1, tcount)?;
                tcount += 1;
                debug!("Yielded token: {:?}", tok);

                f.file.push(tok);
            }
        }
        f.file.reverse();
        f.next = f.file.pop();
        Ok(f)
    }
}

impl fmt::Display for File {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.file)
    }
}

#[cfg(test)]
mod test{

    use super::*;

    #[test]
    fn test_block_comment_content() -> Result<(), AppError>{
        let input = format!("/*\n// block comment\n */ */");
        let output = File::comment_strip(input)?;
        let expected = format!("\n\n */\n");
        assert_eq!(output, expected);
        Ok(())
    }

    #[test]
    fn test_block_comments() -> Result<(), AppError>{
        let input = format!("/* */ */");
        let output = File::comment_strip(input)?;
        let expected = format!(" */\n");
        assert_eq!(output, expected);
        Ok(())
    }

    #[test]
    fn arith_assignments_ext() -> Result<(), AppError>{
        let input = "x = 8 + -2 - -8 * (9 + 1);";
        let expected = "x = 8 + -2 - -8 * ( 9 + 1 ) ;";
        let output = File::insert_space(input)?;
        assert_eq!(output, expected);
        Ok(())
    }

    #[test]
    fn int_arrays() -> Result<(), AppError>{
        let input = "void int_arrays(int x, int* p){ int* a = new int[x]; int y = x % 10; a[7] = a[19+y]; a[6]++; a[6]--;  }";
        let expected = "void int_arrays ( int x , int * p ) { int * a = new int [ x ] ; int y = x % 10 ; a [ 7 ] = a [ 19 + y ] ; a [ 6 ] ++ ; a [ 6 ] -- ; }";
        let output = File::insert_space(input)?;
        assert_eq!(output, expected);
        Ok(())
    }

}

