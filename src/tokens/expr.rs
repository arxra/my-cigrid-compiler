use super::unop::Unop;

#[derive(Debug, Clone)]
pub enum Expr {
    Ident(String),
    Int(isize),
    Char(char),
    String(String),
    BInt(Unop, isize),
}
