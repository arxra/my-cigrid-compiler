use std::fmt;

/// Type identifier, for the next token
#[derive(Debug, Clone)]
pub enum Ty {
    Void,
    Int,
    Char,
    TIdent(String),
    TPoint(Box<Ty>),
}

impl Ty {
    pub fn word(w: &str) -> Option<Self> {
        match w {
            "int" => Some(Self::Int),
            "char" => Some(Self::Char),
            "void" => Some(Self::Void),
            _ => None,
        }
    }
}

impl fmt::Display for Ty {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let st = match self {
            Self::Void => format!("TVoid"),
            Self::Int => format!("TInt"),
            Self::Char => format!("TChar"),
            Self::TIdent(i) => format!("TIdent(\"{}\")", i),
            Ty::TPoint(t) => format!("TPoint({})", t),
        };
        write!(f, "{}", st)
    }
}
