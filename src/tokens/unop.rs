use std::fmt;

#[derive(Debug, Clone)]
pub enum Unop {
    Exc,
    Tilde,
    Dash,
}

impl Unop {
    pub fn new(c: char) -> Option<Self> {
        match c {
            '~' => Some(Self::Tilde),
            '!' => Some(Self::Exc),
            '-' => Some(Self::Dash),
            _ => None,
        }
    }
}

impl fmt::Display for Unop {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let st = match self {
            Self::Exc => {
                format!("!")
            }
            Self::Tilde => {
                format!("~")
            }
            Self::Dash => {
                format!("-")
            }
        };
        write!(f, "{}", st)
    }
}
