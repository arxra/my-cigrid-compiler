use std::fmt;

#[derive(Debug, Clone, Copy)]
pub enum Binop {
    Plus,
    Minus,
    Star,
    FSlash,
    Perc,
    LThan,
    GThan,
    LEThan,
    GEThan,
    DLThan,
    DGThan,
    DEquals,
    NEquals,
    And,
    Bar,
    DAnd,
    DBar,
    DMin,
    DPlus,
}

impl Binop {
    /// Checks the associativity of the operator.
    /// Returns true for left associative operators, which are most
    pub fn lassoc(&self) -> bool {
        match self {
            //Binop::Plus => {}
            //Binop::Minus => {}
            //Binop::Star => {}
            //Binop::FSlash => {}
            //Binop::Perc => {}
            //Binop::LThan => {}
            //Binop::GThan => {}
            //Binop::LEThan => {}
            //Binop::GEThan => {}
            //Binop::DLThan => {}
            //Binop::DGThan => {}
            //Binop::DEquals => {}
            //Binop::NEquals => {}
            //Binop::And => {}
            //Binop::Bar => {}
            //Binop::DAnd => {}
            //Binop::DBar => {}
            _ => {
                info!("Not any associtivity with token, assuming left");
                true
            }
        }
    }
    pub fn prec(&self) -> usize {
        match self {
            Binop::DMin => 11,
            Binop::DPlus => 11,
            Binop::Star => 9,
            Binop::FSlash => 9,
            Binop::Perc => 9,
            Binop::Plus => 8,
            Binop::Minus => 8,
            Binop::DLThan => 7,
            Binop::DGThan => 7,
            Binop::LThan => 6,
            Binop::GThan => 6,
            Binop::LEThan => 6,
            Binop::GEThan => 6,
            Binop::DEquals => 5,
            Binop::NEquals => 5,
            Binop::And => 4,
            Binop::Bar => 3,
            Binop::DAnd => 2,
            Binop::DBar => 1,
        }
    }
    pub fn new(a: char, b: Option<char>) -> Option<Binop> {
        let bin = match (a, b) {
            ('<', None) => Binop::LThan,
            ('<', Some('=')) => Binop::LEThan,
            ('<', Some('<')) => Binop::DLThan,
            ('>', None) => Binop::GThan,
            ('>', Some('=')) => Binop::GEThan,
            ('>', Some('>')) => Binop::DGThan,
            ('%', None) => Binop::Perc,
            ('/', None) => Binop::FSlash,
            ('*', None) => Binop::Star,
            ('+', None) => Binop::Plus,
            ('-', None) => Binop::Minus,
            ('&', None) => Binop::And,
            ('&', Some('&')) => Binop::DAnd,
            ('|', None) => Binop::Bar,
            ('|', Some('|')) => Binop::DBar,
            ('!', Some('=')) => Binop::NEquals,
            ('=', Some('=')) => Binop::DEquals,
            ('-', Some('-')) => Binop::DMin,
            ('+', Some('+')) => Binop::DPlus,
            (_, _) => return None,
        };
        return Some(bin);
    }
}

impl fmt::Display for Binop {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let st: String = match self {
            Self::Plus => {
                format!("+")
            }
            Self::Minus => {
                format!("-")
            }
            Self::Star => {
                format!("*")
            }
            Self::FSlash => {
                format!("/")
            }
            Self::Perc => {
                format!("%")
            }
            Self::LThan => {
                format!("<")
            }
            Self::GThan => {
                format!(">")
            }
            Self::LEThan => {
                format!("<=")
            }
            Self::GEThan => {
                format!(">=")
            }
            Self::DLThan => {
                format!("<<")
            }
            Self::DGThan => {
                format!(">>")
            }
            Self::DEquals => {
                format!("==")
            }
            Self::NEquals => {
                format!("!=")
            }
            Self::And => {
                format!("&")
            }
            Self::Bar => {
                format!("|")
            }
            Self::DAnd => {
                format!("&&")
            }
            Self::DBar => {
                format!("||")
            }
            Self::DPlus | Self::DMin => return Err(fmt::Error),
        };
        write!(f, "{}", st)
    }
}
