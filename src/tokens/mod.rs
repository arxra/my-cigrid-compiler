pub mod binop; // Token: a single operator
pub mod delims;
pub mod expr; // NOT Token: Uses 2 or more tokens
pub mod keywords; // Token: single keyword
pub mod stmt; // NOT Token: Uses 2 or more tokens
pub mod ty; // NOT Tokens: Used to specify types in parser
pub mod unop; //Token: Single type //Token: Tokens without value. Strictly not a identifier

use crate::errors::AppError;

use self::binop::Binop;
use self::delims::Delim;
use self::expr::Expr;
use self::keywords::Kword;
use self::ty::Ty;
use self::unop::Unop;

#[derive(Debug, Clone)]
pub enum Tok {
    Binop(Binop),
    Kword(Kword),
    Unop(Unop),
    Ty(Ty),
    Expr(Expr),
    Delim(Delim),
}

impl PartialEq for Tok {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", &self).eq(&*format!("{:?}", other))
    }
}

/// A parsed Token
///
/// These tokens contain information in where they were parsed, but not context surrounding them.
/// For example, they know where you defined a int, but they don't know if the assignment is
/// valid.
#[derive(Debug, Clone)]
pub struct Token {
    tok: Tok,
    file: String,
    line: usize,
    id: usize,
}

impl PartialEq for Token {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", &self).eq(&*format!("{:?}", other))
    }
}

impl Token {
    pub fn assert_tok(&self, other: Tok) -> Result<(), AppError> {
        if other != self.tok() {
            let mut err = AppError::new(
                1,
                format!(
                    "Token: assert_tok: Was expecting {:?}, found {:?}",
                    other,
                    self.tok()
                ),
            );
            err.line = Some(self.line());
            return Err(err);
        }
        trace!("Token: assert_tok: Next token was indeed {:?}", other);
        Ok(())
    }
    pub fn equals(&self, other: Tok) -> bool {
        self.tok == other
    }
    pub fn line(&self) -> usize {
        self.line
    }
    pub fn tok(&self) -> Tok {
        self.tok.clone()
    }
    pub fn file(&self) -> String {
        self.file.clone()
    }
    // This should ideally have a single token.
    pub fn new(word: &str, file: String, line: usize, id: usize) -> Result<Self, AppError> {
        info!("Tokenizing word: {}", word);
        let mut chars = word.chars().peekable();
        let mut curr = String::new();
        let mut peek: char = ' ';

        //  This gives a word which have one or more possible matches.
        //
        //  Just like in the calculator, for each word, parse it char by char.
        //
        //  This would undoubitidly be more concise with regex.
        //

        while let Some(c) = chars.next() {
            if let Some(p) = chars.peek() {
                peek = *p;
            }
            if (c).is_ascii_digit() || c == '-' && peek != '-' {
                let mut min = 0;
                debug!("Parsing digit: {}", c);
                if c == '-' {
                    if word.len() == 1 {
                        trace!("returning Binop::Minus");
                        return Ok(Token {
                            tok: Tok::Binop(Binop::Minus),
                            file,
                            line,
                            id,
                        });
                    }
                    min = 1;
                }
                if word.len() > min + 1
                    && &word[min + 1..min + 2] == "0"
                    && &word[min..min + 1] == "0"
                {
                    return Err(AppError::new(1, format!("zero cannot follow init zero")));
                }
                match word[min..word.len()].parse::<isize>() {
                    Ok(i) => {
                        if min == 1 {
                            trace!("returning expr::BInt(Unop::Dash, {})", i);
                            return Ok(Token {
                                tok: Tok::Expr(Expr::BInt(Unop::Dash, i)),
                                file,
                                line,
                                id,
                            });
                        } else {
                            trace!("Regular int {} tokenized", i);
                            return Ok(Token {
                                tok: Tok::Expr(Expr::Int(i)),
                                file,
                                line,
                                id,
                            });
                        }
                    }
                    Err(_) => {
                        if let Ok(c) =
                            isize::from_str_radix(word.to_lowercase().trim_start_matches("0x"), 16)
                        {
                            return Ok(Token {
                                tok: Tok::Expr(Expr::Int(c)),
                                file,
                                id,
                                line,
                            });
                        }
                        if c != '-' {
                            return Err(AppError::new(
                                1,
                                format!("Could not parse seemingly integer: ({})", curr),
                            ));
                        }
                    }
                };
            } else if (c).is_ascii_alphabetic() || (c) == '_' {
                debug!("Found char, and some: ({}){:#?}", c, chars);
                debug!("current: {:#?}", curr);
                curr.push(c);
                while let Some(c) = chars.next() {
                    // corresponds to [_a-zA-Z][_a-zA-Z0-9]*
                    if !c.is_ascii_alphabetic() && c != '_' && !c.is_ascii_digit() {
                        break;
                    }
                    curr.push(c);
                }

                if let Some(c) = chars.next() {
                    return Err(AppError::new(
                        1,
                        format!("Found more chars, not done parsing: {}", c),
                    ));
                }
                if let Some(to) = Ty::word(&*curr) {
                    let tok = Tok::Ty(to);
                    return Ok(Token {
                        tok,
                        line,
                        file,
                        id,
                    });
                } else if let Some(to) = Kword::word(&*curr) {
                    let tok = Tok::Kword(to);
                    return Ok(Token {
                        tok,
                        line,
                        id,
                        file,
                    });
                } else {
                    let tok = Tok::Expr(Expr::Ident(curr.clone()));
                    return Ok(Token {
                        tok,
                        line,
                        file,
                        id,
                    });
                }
            } else if c.is_ascii() {
                info!("handling c as not digit, not alpha: {:?}", c);
                // Here we have already tried all ascii alphabetics and digits as well as single
                // dash. Only possilbe remains is for ascii signs outside of these, which are
                // operators we should parse. Since we are parsing a Cigrid sub set, we know these
                // must be ascii compliant as per the specification. The tricky part is figuring
                // out which signs they are, in a neat fashion.
                let tok = match (c, chars.next()) {
                    ('"', Some(c)) => {
                        /*"*/
                        
                        let mut res = String::from(c);
                        let mut b = Some(' ');

                        #[allow(irrefutable_let_patterns)]
                        // We are going to parse untill None and then break in the worst case
                        while let a = chars.next() {
                            trace!("Parsing a({:?}) with b({:?}))", a, b);
                            if a == Some('"') && b != Some('\\') {
                                /*"*/
                                
                                break;
                            } else if a != None {
                                res.push(a.unwrap());
                            } else {
                                return Err(AppError::new(
                                    1,
                                    format!(
                                        "Unable to parse String ({:?} with {:?}) in {}:{}",
                                        c, a, file, line
                                    ),
                                ));
                            }
                            b = a;
                        }
                        Tok::Expr(Expr::String(res))
                    }
                    ('\'', Some(a)) => {
                        trace!("Handling escaped char: a={}", a);
                        // Parse char, possibly escaped
                        // example: \'\\\'\'
                        // a = \
                        // b = \'
                        // c = \'
                        // Expected return: Tok::Esc(Esc.new(b))
                        let b = chars.next();
                        trace!("Possible end if not escaped: b={:?}", b);

                        if a == '\\' {
                            let c = chars.next();
                            trace!("it was escaped, but is it matched?: c={:?}", c);
                            if c != Some('\'') {
                                return Err(AppError::new(
                                        1,
                                        format!("Unable to parse escaped apostroph in {}:{}  as it match was {:?}", file, line, c),
                                ));
                            }

                            // handle t and n seperatly, otherwise just put it in.
                            if let Some('t') = b {
                                Tok::Expr(Expr::Char('\t'))
                            } else if let Some('n') = b {
                                Tok::Expr(Expr::Char('\n'))
                            } else if let Some(d) = b {
                                warn!("Expected case used");
                                Tok::Expr(Expr::Char(d)) // Expected
                            } else {
                                return Err(AppError::new(
                                        1,
                                        format!("Unable to parse escaped apostroph in {}:{} as there was no more chars to parse",
                                            file, line),
                                ));
                            }
                        } else if a != '"' {
                            Tok::Expr(Expr::Char(a))
                        } else {
                            return Err(AppError::new(
                                1,
                                format!("Qoute signs must be manually escaped!"),
                            ));
                        }
                    }
                    (a, b) => {
                        trace!("Found possible operator: ({}, {:?})", a, b);
                        if let Some(c) = Binop::new(a, b) {
                            Tok::Binop(c)
                        } else if let Some(c) = Delim::new(a) {
                            Tok::Delim(c)
                        } else if let Some(c) = Unop::new(a) {
                            Tok::Unop(c)
                        } else {
                            return Err(AppError::new(
                                1,
                                format!(
                                    "Unable to parse valid ascii sign ({:?} with {:?}) in {}:{}",
                                    a, b, file, line
                                ),
                            ));
                        }
                    }
                };
                let token = Token {
                    tok,
                    id,
                    line,
                    file,
                };
                return Ok(token);
            } else {
                warn!("Could not parse c: {:?}", c);
                return Err(AppError::new(
                    1,
                    format!("Do not know how to scan token {:?}", c),
                ));
            }
        }
        Err(AppError::new(1, String::from("Tried to parse empty token")))
    }
}
