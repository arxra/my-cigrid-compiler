#[derive(Debug, Clone)]
pub enum Kword {
    Break,
    Extern,
    New,
    While,
    For,
    Return,
    Delete,
    If,
    Struct,
    Else,
    Int,
    Char,
    Void,
}

impl Kword {
    pub fn word(w: &str) -> Option<Self> {
        match w {
            "break" => Some(Self::Break),
            "extern" => Some(Self::Extern),
            "new" => Some(Self::New),
            "while" => Some(Self::While),
            "for" => Some(Self::For),
            "return" => Some(Self::Return),
            "delete" => Some(Self::Delete),
            "if" => Some(Self::If),
            "else" => Some(Self::Else),
            "struct" => Some(Self::Struct),
            "int" => Some(Self::Int),
            "char" => Some(Self::Char),
            "void" => Some(Self::Void),
            _ => None,
        }
    }
}
