#[derive(Debug, Clone, PartialEq)]
pub enum Delim {
    LPar,
    RPar,
    LCurl,
    RCurl,
    LBrack,
    RBrack,
    Dot,
    Comma,
    SemiCol,
    Eq,
}

impl Delim {
    pub fn new(c: char) -> Option<Self> {
        match c {
            '(' => Some(Self::LPar),
            ')' => Some(Self::RPar),
            '{' => Some(Self::LCurl),
            '}' => Some(Self::RCurl),
            '[' => Some(Self::LBrack),
            ']' => Some(Self::RBrack),
            '.' => Some(Self::Dot),
            ',' => Some(Self::Comma),
            ';' => Some(Self::SemiCol),
            '=' => Some(Self::Eq),
            _ => None,
        }
    }
}
