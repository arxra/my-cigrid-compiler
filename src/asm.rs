use std::fmt;

use crate::asm_ir::{Inst, Register};
#[allow(unused_imports)]
use crate::{
    asm_ir::{AsmIR, Asmline, Env},
    errors::AppError,
    hybrid_ir::{Blockend, IRFunc, IRProg, IRStmt, Sym},
    parseparts::e::E,
    tokens::binop::Binop,
    tokens::ty::Ty,
};

#[derive(Debug, Clone)]
pub struct Asm {
    lines: Vec<Asmline>,
}

impl Asm {
    /// Converts AsmIR to Asm object, which should be executable after linking.
    /// for example, below code should be inserted below the main label if we need 32 bytes of
    /// memory for our program.
    /// ```asm
    /// push    rbp
    /// mov     rbp, rsp
    /// sub     rsp, 32
    /// ```
    pub fn new(asmir: &mut AsmIR) -> Result<Self, AppError> {
        let rbp = Register::new(Sym::new(String::from("rbp")), None, None);
        let rsp = Register::new(Sym::new(String::from("rsp")), None, None);
        let r10 = Register::new(Sym::new(String::from("r10")), None, None);
        for (num, line) in asmir.insts.clone().iter().enumerate() {
            if let Asmline::Label(label) = line {
                if let "main" = &**label {
                    // insert the memory allocations
                    let save_rbp = Asmline::Inst(Inst::Push, Some(rbp.clone()), None);
                    asmir.insts.insert(num + 1, save_rbp);
                    let save_rsp = Asmline::Inst(Inst::Mov, Some(rbp.clone()), Some(rsp.clone()));
                    asmir.insts.insert(num + 2, save_rsp);

                    let save_sub = Asmline::Assign(r10.clone(), asmir.env.totbuf);
                    asmir.insts.insert(num + 3, save_sub);

                    let create_space =
                        Asmline::Inst(Inst::Sub, Some(rsp.clone()), Some(r10.clone()));
                    asmir.insts.insert(num + 4, create_space);

                    let retsym = Sym::new(String::from("return_reg"));
                    Asmline::Assign(asmir.env.get_reg(&retsym)?, 0);
                }
            }
            match line {
                Asmline::Inst(Inst::Ret, None, None) => {
                    let save_rsp = Asmline::Inst(Inst::Mov, Some(rbp.clone()), Some(rsp.clone()));
                    asmir.insts.insert(num + 4, save_rsp);

                    let save_add = Asmline::Assign(r10.clone(), asmir.env.totbuf);
                    asmir.insts.insert(num + 5, save_add);

                    let create_space =
                        Asmline::Inst(Inst::Add, Some(rsp.clone()), Some(r10.clone()));
                    asmir.insts.insert(num + 6, create_space);

                    let pop_rbp = Asmline::Inst(Inst::Pop, Some(rbp.clone()), None);
                    asmir.insts.insert(num + 7, pop_rbp);
                }
                other => {
                    trace!("skipping processing of {}", other)
                }
            }
        }

        debug!("{}", asmir.env);

        info!("Asm: \n{}", asmir);

        let asm = Asm {
            lines: asmir.insts.clone(),
        };
        Ok(asm)
    }
}

impl fmt::Display for Asm {
    /// Custom display for Asm objects
    ///
    /// # Arguments
    ///
    /// * `f` - the formatter
    ///
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.lines.clone() {
            write!(f, "{}", line)?;
        }
        Ok(())
    }
}
