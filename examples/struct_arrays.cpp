// Access and updates of structures and arrays (G)
void struct_arrays(){ 
  S2 * a1 = new S2[5]; 
  a1[2].x = 3;
  a1[2].x++;
  a1[2].x--;
  int y = a1[2].y + 1;
  delete[] a1;
}
