// For loops, with and with out scopes + break (G)
// Should be encoded as syntactic sugar
int for_loop_with_break(int x, int y){
  for(int i=0; i < x; i++){
    if(i >= y)
      break;
  }
  for(int i=0; i < x; i++)
    x = x + 1;
 
  return x;
}
