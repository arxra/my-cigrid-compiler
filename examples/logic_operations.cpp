// Various logical operations (S)
void logic_operations(int x, int y, int z){
  x = y | z & 7; 
  x = y || z && y | 1;
  y = x > y && y < x || z >= y && ((x <= y) == 0);
  z = x == y || x != z;
}
