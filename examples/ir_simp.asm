	extern	putchar
	global	main
	section	.text
main:
	push	rbp
	mov	rbp,	rsp
	mov	r10,	32
	sub	rsp,	r10
	mov	qword [rbp-16],	1
	mov	qword [rbp-24],	5
	mov	qword [rbp-32],	4
	mov	r10,	qword [rbp-32]
	add	qword [rbp-16],	r10
	mov	r10,	qword [rbp-24]
	mov	qword [rbp-32],	r10
	mov	r10,	qword [rbp-32]
	add	qword [rbp-16],	r10
	mov	rdi,	65
	call	putchar
	mov	rdi,	qword [rbp-16]
	call	putchar
	mov	rax,	0
	mov	rbp,	rsp
	mov	r10,	32
	add	rsp,	r10
	pop	rbp
	ret

