// If statement and if-then-else statement (S)
int if_then_else(int x, int y){
  if(x == y)
    return y;
  
  if(x > 0){
    if(x == 3){
      x = x + 1;
      return x;
    }
  }
  else{
    x = 7;
    return x + 1;
  }
  return x;
}

// Correct parsing of dangling else (S)
int if_then_dangeling_else(int x){
  if(x > 0)
    if(x < 10)
      return x + 1;
    else
      return x + 2;
  return x;
}
