// Print a signed integer in decimal form (S)
void print_int(int x){
  if(x < 0){
    putchar('-');
    x = x * (0 - 1);  
  }
  int i = 1000000;
  while(i != 0){
    if(x >= i || (x == 0 && i == 1))
      putchar('0' + x / i);
    x = x % i;
    i = i / 10;
  }
}
