// Test print inorder traversal (G)
void test_recursive_data_structures(){
  print_string("---test-recursive-data-structures---\n");
  Tree* t = node(node(leaf(1), 2, leaf(3)), 4, leaf(5));
  inorder_traversal(t);
  putchar('\n');
  delete_tree(t);
}
