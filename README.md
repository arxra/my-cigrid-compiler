# Small Cigrid compiler

This small compiler is made to handle the Cigrid made up langugage specification, a made upp subset of CPP for the course
ID2202 - Compilers and execution environments at KTH.

Working example:

![alt text](img/1611402711_screenshot.png "assembled code")

## Running

This is a rust project, you should be able to get a `cigrid` executable by running `make` on this project.

For the tests, `cargo test` will run all the tests. Note: they do not all pass as some tests are made for a higher
grade than I ended up implementing. Each test runns different files in the examples folder, where the `*.cpp` files
are input data and the same name ending in `.txt` is the expected output data.

The largest testfile which produces assembler is `ir_simp.cpp`. Its output can be compiled as followed:

- `./cigrid examples/ir_simp.cpp > test.ir`
- `nasm -felf64 -g test.ir -o test.o`
- `gcc -no-pie test.o -o test`

which will create a `test` executable file performing the actions in ir_simp.
The same workflow as above is done with `make test_asm`, which also exectures the file.

## Cigrid

The Cigrid specification was quite easy to follow once you knew what you were trying to do.
I first tried to implement the internal Rust structers based on the complete grammar of the specification, which turned
out to be a huge waste of resources. Once I instead started using the abstrac syntax defenition of the lanugage it went
a whole lot smoother.

## Regex (Or lack there of)

I learned to late that we were allowed to use regexes for parsing out tokens.
This would have saved about half the code in the project, as the scanner module is almost entirely just handling of
different tokens manually. I estiamte my code would been reduced by 1800 lines if I did not implement my own token
parser completly from scratch and instead used regex matches. It is also a much better skill to have under your belt
to know regex than manually parsing tokens.
The worst part of the token parser is the 3 passes it does over the code:

- Remove all comments
- Insert spaces around valid tokens
- Match tokens to my Rust enums

This would have been a single regex-loop over my code if I was aware we could use regex, but due to me missunderstanding
I did it the manuall way. This was a huge sunken cost falicity, as I had spent 3-4 days on my manuall implementation
by the time I learned we could use regex. I am in post sure it would have been faster to throw my code out and use
regexs instead...

## Rust

One of the available languages for the course was Rust, a language I have followed for quite a while before the course
started. In fact, it was in part the premise of using Rust in the course which made me switch over together with
the examiner being David Broman. This course was a lot of hands on with Rust and here are some of the things I have
learned.

### Traits

Rust has the notion of traits to restrict generics or extending structs and enums.
This makes it really easy to get a lot of functionallity on your own implementations.
Below in fmt::Display I'm praising the debug printout of all my types, getting it is as easy as
deriving the `Debug` trait to your types. Really handy and helps a lot with reducing boiler plate code.

### `Vec<Box<T>>`

If you know what both of Vec and Box means, you realize how stupid the above statement is.
A Vec in Rust is a type from the standard library, a heap-allocated vector.
Box indicates that its a pointer to something on the heap.
So a `Vec<Box<T>>` is a vector on the heap of pointers to heap allocated objects.
The box just adds another layer of indiraction which serves no purpose.

### Enums

Enums in Rust are super powerfull and usefull.
While building a compiler, converting everything between enums as the intermediate representation changes is a breeze
and you know you always get it right. At least its super obvious when its not.

### Testing

Cargo's built in testing framework is great. While writing the selfmade token parser, having unit tests for everything
that had previously failed made fixing the next bug fearlessly simple, as one was sure no new bugs were introduced.
This also allowed for some test driven development when the compiler gave the wrong output in the course testrunner
which gave its input and expected output. This is why there are so many files in the examples folder, as I wanted not
only automated test but be able to quickly run separate test individually. And yes, Cargo allows this but sometimes
I just wanted to use my generated binary to run a single testfile you know?

### Crate - envlogger

This might be the best library I have ever used. It made it super simple to implement each part of the compiler with
plenty of debugging available when I needed it. The automatic tagging of which module which was logging also made it
easy to find what I was looking for.

### Crate - Clap

Clap is the well known Rust crate for handling command line arguments. This crate probably saved tens of hours of
command line parsing and was very ergonomic to use. Look forward to clap 3.0, which is supposedly even more ergonomic.

### ftm::Display

Leaving everything as Rust enums and structs, always, made it easy to work with the code as I always had strong type
guarantees. When I wanted output from the compiler, each enum and struct simply had custom formatters and I could
just print the Rust "object" I had and the Rust formatter would correctly pretty print all my data. Very handy.
This while I still had the option to log all the data in case my custom formatter missed something (which happend) by
`debug!("{:?}", somestruct)`.
