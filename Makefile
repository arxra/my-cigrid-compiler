.PHONY: all clean
all:
	cargo build --release
	cp target/release/cigrid cigrid

clean:
	cargo clean
	rm cigrid

test_asm:
	RUST_LOG=trace cargo run examples/ir_simp.cpp --asm > test.asm
	nasm -felf64 -g test.asm -o test.o
	gcc -no-pie test.o -o test
	./test
